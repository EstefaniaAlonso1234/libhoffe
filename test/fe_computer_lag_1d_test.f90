module fe_computer_lag_1d_test_module

  use fruit !! Fortran Unit Test Framework
  use nrtypes !! Numeric types
  use fe_traits_1d, only: fe_traits => lagrange_fe_traits
  use quad_rule, only: qr_gauss_2pt
  use mesh_1d, only: Mesh1d !! 1-d mesh
  use lagrange_fe_1d, only: LagrangeFe1d
  use fe_computer_lagrange_1d, only:&
       MassMatrixComputer => MassMatrixComputer_Lag,&
       StiffnessMatrixComputer => StiffnessMatrixComputer_Lag,&
       RhsVectorComputer => RhsVectorComputer_Lag
  implicit none
  private
  public fe_computer_lag_1d_test

contains

  subroutine fe_computer_lag_1d_test
    write (*,'(A)',advance="no") "[fe_computer_lag_1d_test]"
    call test_mass_matrix
    call test_stiffness_matrix
    call test_rhs_vector
  end subroutine fe_computer_lag_1d_test

  subroutine test_mass_matrix
    type(Mesh1d), target :: Th
    type(LagrangeFe1d), target :: fe
    type(MassMatrixComputer) :: fe_computer
    real(dp), parameter :: a=0.0_dp, b=1.0_dp
    integer(long), parameter :: ncells = 1

    Th = Mesh1d(a, b, ncells)
    fe = LagrangeFe1d(Th)
    fe_computer = MassMatrixComputer(fe, quad_rule=qr_gauss_2pt)
    call fe%reinit(icell=1, order=1)
    call fe_computer%reinit()
    call fe_computer%compute()
    call assert_equals(reshape(fe_computer%local_matrix, [4]),&
         [0.3333333333333333_dp, 0.1666666666666666_dp,&
          0.1666666666666666_dp, 0.3333333333333333_dp], 4, delta=1d-16 )
  end subroutine test_mass_matrix

  subroutine test_stiffness_matrix
    type(Mesh1d), target :: Th
    type(LagrangeFe1d), target :: fe
    type(StiffnessMatrixComputer) :: fe_computer
    real(dp), parameter :: a=0.0_dp, b=1.0_dp
    integer(long), parameter :: ncells=1

    Th = Mesh1d(a, b, ncells)
    fe = LagrangeFe1d(Th)
    fe_computer = StiffnessMatrixComputer(fe, quad_rule=qr_gauss_2pt)
    call fe%reinit(icell=1, order=1)
    call fe_computer%reinit()
    call fe_computer%compute()
    call assert_equals(reshape(fe_computer%local_matrix, [4]),&
         [1.0_dp, -1.0_dp,&
         -1.0_dp, 1.0_dp], 4, delta=1d-16 )
  end subroutine test_stiffness_matrix

  function func(x)
    ! Function f(x)=x^2, to be used by test_rhs_vector
    real(dp), intent(in) :: x
    real(dp) func
    func = x**2
  end function func

  subroutine test_rhs_vector
    type(Mesh1d), target :: Th
    type(LagrangeFe1d), target :: fe
    type(RhsVectorComputer) :: fe_computer
    real(dp), parameter :: a=0.0_dp, b=2.0_dp
    integer(long), parameter :: ncells=2
    integer(long) :: n
    real(dp), dimension(2,2), parameter :: expected_result = &
         reshape([ 8.33333333333333d-002, 0.25_dp,&
         0.9166666666666666_dp, 1.416666666666666_dp], [2, 2] )

    Th = Mesh1d(a, b, ncells)
    fe = LagrangeFe1d(Th)
    fe_computer = RhsVectorComputer(fe, quad_rule=qr_gauss_2pt)
    do n=1,ncells
       call fe%reinit(icell=n, order=1)
       call fe_computer%reinit_function(func)
       call fe_computer%compute()
       call assert_equals(expected_result(:,n), fe_computer%local_vector, 2, delta=1d-15)
    end do
  end subroutine test_rhs_vector


end module fe_computer_lag_1d_test_module

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! end:
