! Copyright (c) 2005-2010, 2012-2013, Andrew Hang Chen and contributors,
! All rights reserved.
! Licensed under the 3-clause BSD license.

program fruit_driver
  use fruit

  !,--------------------------
  !| Load modules to be tested
  !`--------------------------
  use mesh_test_module
  use fe_1d_test_module
  use qr_1d_test_module
  use fe_computer_lag_1d_test_module
  use fe_computer_lob_1d_test_module
  use fe_computer_leg_1d_test_module
  use fe_connectivity_1d_test_module

  !,----------
  !| Run tests
  !`----------
  call init_fruit

  call mesh_test
  call fe_1d_test
  call qr_1d_test
  call fe_computer_lag_1d_test ! Lagrange FE test
  call fe_computer_lob_1d_test ! Lobatto FE test
  call fe_computer_leg_1d_test ! Legendre FE test
  call fe_connectivity_1d_test

  !!,---------
  !!| Finalize
  !!`---------
  call fruit_summary
  call fruit_finalize
  if (fruit_if_case_failed()) then
     stop 1
  else
     stop 0
  endif

end program fruit_driver
