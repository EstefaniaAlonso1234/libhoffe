module qr_1d_test_module

  use fruit !! Fortran Unit Test Framework
  use nrtypes !! Numeric types
  use affine_map_1d, only: AffineMap1d !! Affine transformation of interval
  use quad_rule, only: qr_midpoint !! Quadrature formula in [-1,1]
  implicit none

contains
  pure elemental function f(x)
    real(dp), intent(in) :: x
    real(dp) :: f
    f = 2*x+2
  end function f

  subroutine qr_1d_test
    real(dp), dimension(1) :: data = [1] ! Data to be integrated
    real(dp) :: integral ! Result of integration
    type(AffineMap1d) :: am

    write (*,'(A)',advance="no") "[qr_1d_test]"

    ! 1. Test integral of data
    integral = qr_midpoint%integrate(data) ! Integrate data in [-1,1]
    call assert_equals(integral, 2.d0)

    ! 2. Test integral of function
    data = f(qr_midpoint%nodes())
    integral = qr_midpoint%integrate(data) ! Integrate f in [-1,1]
    associate(a=>1._dp, b=>-1._dp)
      call assert_equals( integral, (b-a)*(f(b)-f(a))/2. ) ! Base*altura/2
    end associate

    ! 3. Test integration in a domain defined by an affine map
    am = AffineMap1d(0.5_dp, 0.5_dp) ! x = T(ref_x) = a*ref_x + b : [-1,1] -> [0,1]
    call assert_equals(am%det_J(), 0.5_dp)
    data = [1._dp]
    integral = qr_midpoint%integrate(data, am)
    call assert_equals(integral, 1._dp) ! Base = [0,1], altura = 1, base*altura
  end subroutine qr_1d_test

end module qr_1d_test_module

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! End:
