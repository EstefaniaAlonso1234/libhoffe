module fe_computer_lob_1d_test_module

  use fruit !! Fortran Unit Test Framework
  use nrtypes !! Numeric types
  use fe_traits_1d, only: fe_traits => lobatto_fe_traits
  use quad_rule, only: qr_gauss_2pt, qr_gauss_3pt, qr_gauss_4pt, qr_gauss_5pt
  use mesh_1d, only: Mesh1d !! 1-d mesh
  use lobatto_fe_1d, only: LobattoFe1d
  use fe_computer_lobatto_1d, only:&
       MassMatrixComputer => MassMatrixComputer_Lob,&
       StiffnessMatrixComputer => StiffnessMatrixComputer_Lob,&
       RhsVectorComputer => RhsVectorComputer_Lob
  implicit none
  private
  public fe_computer_lob_1d_test

contains

  subroutine fe_computer_lob_1d_test
    write (*,'(A)',advance="no") "[fe_computer_lob_1d_test]"
    call test_mass_matrix
    call test_stiffness_matrix_ord1
    call test_rhs_vector
    call test_stiffness_matrix_ord2
    call test_stiffness_matrix_ord3
    !call test_stiffness_matrix_ord5
  end subroutine fe_computer_lob_1d_test

  subroutine test_mass_matrix
    type(Mesh1d), target :: Th
    type(LobattoFe1d), target :: fe
    type(MassMatrixComputer) :: fe_computer
    real(dp), parameter :: a=0.0_dp, b=1.0_dp
    integer(long), parameter :: ncells = 1

    Th = Mesh1d(a, b, ncells)
    fe = LobattoFe1d(Th)
    fe_computer = MassMatrixComputer(fe, quad_rule=qr_gauss_2pt)
    call fe%reinit(icell=1, order=1)
    call fe_computer%reinit()
    call fe_computer%compute()
    call assert_equals(reshape(fe_computer%local_matrix, [4]),&
         [0.3333333333333333_dp, 0.1666666666666666_dp,&
          0.1666666666666666_dp, 0.3333333333333333_dp], 4, delta=1d-16 )
  end subroutine test_mass_matrix

  subroutine test_stiffness_matrix_ord1
    type(Mesh1d), target :: Th
    type(LobattoFe1d), target :: fe
    type(StiffnessMatrixComputer) :: fe_computer
    real(dp), parameter :: a=0.0_dp, b=1.0_dp
    integer(long), parameter :: ncells=1

    Th = Mesh1d(a, b, ncells)
    fe = LobattoFe1d(Th)
    fe_computer = StiffnessMatrixComputer(fe, quad_rule=qr_gauss_3pt)
    call fe%reinit(icell=1, order=1)
    call fe_computer%reinit()
    call fe_computer%compute()
    call assert_equals(reshape(fe_computer%local_matrix, [4]),&
         [1.0_dp, -1.0_dp,&
         -1.0_dp, 1.0_dp], 4, delta=1d-15 )
  end subroutine test_stiffness_matrix_ord1

  function func(x)
    ! Function f(x)=x^2, to be used by test_rhs_vector
    real(dp), intent(in) :: x
    real(dp) func
    func = x**2
  end function func

  subroutine test_rhs_vector
    type(Mesh1d), target :: Th
    type(LobattoFe1d), target :: fe
    type(RhsVectorComputer) :: fe_computer
    real(dp), parameter :: a=0.0_dp, b=2.0_dp
    integer(long), parameter :: ncells=2
    integer(long) :: n
    real(dp), dimension(2,2), parameter :: expected_result = &
         reshape([ 8.33333333333333d-002, 0.25_dp,&
         0.9166666666666666_dp, 1.416666666666666_dp], [2, 2] )

    Th = Mesh1d(a, b, ncells)
    fe = LobattoFe1d(Th)
    fe_computer = RhsVectorComputer(fe, quad_rule=qr_gauss_2pt)
    do n=1,ncells
       call fe%reinit(icell=n, order=1)
       call fe_computer%reinit_function(func)
       call fe_computer%compute()
       call assert_equals(expected_result(:,n), fe_computer%local_vector, 2, delta=1d-15)
    end do
  end subroutine test_rhs_vector

  subroutine test_stiffness_matrix_ord2
    type(Mesh1d), target :: Th
    type(LobattoFe1d), target :: fe
    type(StiffnessMatrixComputer) :: fe_computer
    real(dp), parameter :: a=0.0_dp, b=1.0_dp
    integer(long), parameter :: ncells=1

    Th = Mesh1d(a, b, ncells)
    fe = LobattoFe1d(Th)
    fe_computer = StiffnessMatrixComputer(fe, quad_rule=qr_gauss_2pt)
    call fe%reinit(icell=1, order=2)
    call fe_computer%reinit()
    call fe_computer%compute()
    associate(A => fe_computer%local_matrix)
      call assert_equals(dble([ 1,-1, 0]), dble(A(:,1)), 3, delta=1d-14)
      call assert_equals(dble([-1, 1, 0]), dble(A(:,2)), 3, delta=1d-14)
      call assert_equals(dble([ 0, 0, 2]), dble(A(:,3)), 3, delta=1d-14)
    end associate
  end subroutine test_stiffness_matrix_ord2
  subroutine test_stiffness_matrix_ord3
    type(Mesh1d), target :: Th
    type(LobattoFe1d), target :: fe
    type(StiffnessMatrixComputer) :: fe_computer
    real(dp), parameter :: a=0.0_dp, b=1.0_dp
    integer(long), parameter :: ncells=1

    Th = Mesh1d(a, b, ncells)
    fe = LobattoFe1d(Th)
    fe_computer = StiffnessMatrixComputer(fe, quad_rule=qr_gauss_3pt)
    call fe%reinit(icell=1, order=3)
    call fe_computer%reinit()
    call fe_computer%compute()
    associate(A => fe_computer%local_matrix)
      call assert_equals(dble([ 1,-1, 0, 0]), dble(A(:,1)), 3, delta=1d-14)
      call assert_equals(dble([-1, 1, 0, 0]), dble(A(:,2)), 3, delta=1d-14)
      call assert_equals(dble([ 0, 0, 2, 0]), dble(A(:,3)), 3, delta=1d-14)
      call assert_equals(dble([ 0, 0, 0, 2]), dble(A(:,4)), 3, delta=1d-14)
    end associate
  end subroutine test_stiffness_matrix_ord3

  ! subroutine test_stiffness_matrix_ord5
  !   type(Mesh1d), target :: Th
  !   type(LobattoFe1d), target :: fe
  !   type(StiffnessMatrixComputer) :: fe_computer
  !   real(dp), parameter :: a=0.0_dp, b=1.0_dp
  !   integer(long), parameter :: ncells=1

  !   Th = Mesh1d(a, b, ncells)
  !   fe = LobattoFe1d(Th)
  !   fe_computer = StiffnessMatrixComputer(fe, quad_rule=qr_gauss_5pt)
  !   call fe%reinit(icell=1, order=5)
  !   call fe_computer%reinit()
  !   call fe_computer%compute()
  !   associate(A => fe_computer%local_matrix)
  !     call assert_equals(dble([ 1,-1, 0, 0, 0, 0]), dble(A(:,1)), 5, delta=1d-14)
  !     call assert_equals(dble([-1, 1, 0, 0, 0, 0]), dble(A(:,2)), 5, delta=1d-14)
  !     call assert_equals(dble([ 0, 0, 2, 0, 0, 0]), dble(A(:,3)), 5, delta=1d-14)
  !     call assert_equals(dble([ 0, 0, 0, 2, 0, 0]), dble(A(:,4)), 5, delta=1d-14)
  !     call assert_equals(dble([ 0, 0, 0, 0, 2, 0]), dble(A(:,5)), 5, delta=1d-14)
  !     call assert_equals(dble([ 0, 0, 0, 0, 0, 2]), dble(A(:,6)), 5, delta=1d-14)
  !   end associate
  ! end subroutine test_stiffness_matrix_ord5

end module fe_computer_lob_1d_test_module

! Local Variables
! flycheck-gfortran-include-path: ("../src")
! end:
