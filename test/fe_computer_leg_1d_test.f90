module fe_computer_leg_1d_test_module

  use fruit !! Fortran Unit Test Framework
  use nrtypes !! Numeric types
  use fe_traits_1d, only: fe_traits => legendre_fe_traits
  use quad_rule, only: qr_gauss_2pt, qr_gauss_3pt, qr_gauss_4pt, qr_gauss_5pt
  use mesh_1d, only: Mesh1d !! 1-d mesh
  use legendre_fe_1d, only: LegendreFe1d
  use fe_computer_legendre_1d, only:&
       MassMatrixComputer => MassMatrixComputer_leg,&
       StiffnessMatrixComputer => StiffnessMatrixComputer_leg,&
       RhsVectorComputer => RhsVectorComputer_leg
  implicit none
  private
  public fe_computer_leg_1d_test

contains

  subroutine fe_computer_leg_1d_test
    write (*,'(A)',advance="no") "[fe_computer_leg_1d_test]"
    ! call test_mass_matrix
    ! call test_stiffness_matrix_ord1
    ! call test_rhs_vector
    call test_mass_matrix_ord2
    !call test_stiffness_matrix_ord3
  end subroutine fe_computer_leg_1d_test

  ! subroutine test_mass_matrix
  !   type(Mesh1d), target :: Th
  !   type(LegendreFe1d), target :: fe
  !   type(MassMatrixComputer) :: fe_computer
  !   real(dp), parameter :: a=0.0_dp, b=1.0_dp
  !   integer(long), parameter :: ncells = 1

  !   Th = Mesh1d(a, b, ncells)
  !   fe = LegendreFe1d(Th)
  !   fe_computer = MassMatrixComputer(fe, quad_rule=qr_gauss_2pt)
  !   call fe%reinit(icell=1, order=1)
  !   call fe_computer%reinit()
  !   call fe_computer%compute()
  !   call assert_equals(reshape(fe_computer%local_matrix, [4]),&
  !        [0.3333333333333333_dp, 0.1666666666666666_dp,&
  !         0.1666666666666666_dp, 0.3333333333333333_dp], 4, delta=1d-16 )
  ! end subroutine test_mass_matrix

  ! subroutine test_mass_matrix_ord1
  !   type(Mesh1d), target :: Th
  !   type(LegendreFe1d), target :: fe
  !   type(StiffnessMatrixComputer) :: fe_computer
  !   real(dp), parameter :: a=0.0_dp, b=1.0_dp
  !   integer(long), parameter :: ncells=1

  !   Th = Mesh1d(a, b, ncells)
  !   fe = LegendreFe1d(Th)
  !   fe_computer = StiffnessMatrixComputer(fe, quad_rule=qr_gauss_3pt)
  !   call fe%reinit(icell=1, order=1)
  !   call fe_computer%reinit()
  !   call fe_computer%compute()
  !   call assert_equals(reshape(fe_computer%local_matrix, [4]),&
  !        [1.0_dp, -1.0_dp,&
  !        -1.0_dp, 1.0_dp], 4, delta=1d-15 )
  ! end subroutine test_stiffness_matrix_ord1

  function func(x)
    ! Function f(x)=x^2, to be used by test_rhs_vector
    real(dp), intent(in) :: x
    real(dp) func
    func = x**2
  end function func

  ! subroutine test_rhs_vector
  !   type(Mesh1d), target :: Th
  !   type(LegendreFe1d), target :: fe
  !   type(RhsVectorComputer) :: fe_computer
  !   real(dp), parameter :: a=0.0_dp, b=2.0_dp
  !   integer(long), parameter :: ncells=2
  !   integer(long) :: n
  !   real(dp), dimension(2,2), parameter :: expected_result = &
  !        reshape([ 8.33333333333333d-002, 0.25_dp,&
  !        0.9166666666666666_dp, 1.416666666666666_dp], [2, 2] )

  !   Th = Mesh1d(a, b, ncells)
  !   fe = LegendreFe1d(Th)
  !   fe_computer = RhsVectorComputer(fe, quad_rule=qr_gauss_2pt)
  !   do n=1,ncells
  !      call fe%reinit(icell=n, order=1)
  !      call fe_computer%reinit_function(func)
  !      call fe_computer%compute()
  !      call assert_equals(expected_result(:,n), fe_computer%local_vector, 2, delta=1d-15)
  !   end do
  ! end subroutine test_rhs_vector

  subroutine test_mass_matrix_ord2
    type(Mesh1d), target :: Th
    type(LegendreFe1d), target :: fe
    type(MassMatrixComputer) :: fe_computer
    real(dp), parameter :: a=0.0_dp, b=1.0_dp
    integer(long), parameter :: ncells=1

    Th = Mesh1d(a, b, ncells)
    fe = LegendreFe1d(Th)
    fe_computer = MassMatrixComputer(fe, quad_rule=qr_gauss_3pt)
    call fe%reinit(icell=1, order=2)
    call fe_computer%reinit()
    call fe_computer%compute()
    associate(A => fe_computer%local_matrix)
      call assert_equals(dble([ 1.,          0., 0.]), dble(A(:,1)), 3, delta=1d-14)
      call assert_equals(dble([ 0._dp, 1._dp/3., 0._dp]), dble(A(:,2)), 3, delta=1d-14)
      call assert_equals(dble([ 0._dp,    0._dp, 1._dp/5.]), dble(A(:,3)), 3, delta=1d-14)
    end associate
  end subroutine test_mass_matrix_ord2
end module fe_computer_leg_1d_test_module

! Local Variables
! flycheck-gfortran-include-path: ("../src")
! end:
