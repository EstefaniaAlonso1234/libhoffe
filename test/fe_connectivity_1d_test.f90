module fe_connectivity_1d_test_module

  use fruit !! Fortran Unit Test Framework
  use nrtypes !! Numeric types
  use fe_traits_1d, only: fe_traits => lagrange_fe_traits
  use quad_rule, only: qr_gauss_2pt
  use mesh_1d, only: Mesh1d !! 1-d mesh
  use lagrange_fe_1d, only: LagrangeFe1d
  use fe_connectivity, only: FeConnectivity
  implicit none
  private
  public fe_connectivity_1d_test

contains

  subroutine fe_connectivity_1d_test
    write (*,'(A)',advance="no") "[fe_connectivity_1d_test]"
    call simple_connectivity_test
    call fe_connectivity_test
  end subroutine fe_connectivity_1d_test

  subroutine simple_connectivity_test
    type(FeConnectivity), dimension(2) :: c
    call c(1)%set_values([-1, 2]) ! Assing array of values
    call c(1)%set_values([-1, 2, 3]) ! Reallocate and assign new array
    call c(2)%set_values([1, 2, 3, -1]) ! Assign more values
    call assert_equals(c(1)%values, [-1, 2, 3], 3)
    call assert_equals(c(2)%values, [1, 2, 3, -1], 4)
  end subroutine simple_connectivity_test

  subroutine fe_connectivity_test
    type(Mesh1d), target :: Th
    type(LagrangeFe1d) :: fe
    real(dp), parameter :: a=0.0, b=1.0
    integer(long), parameter :: ncells=3
    type(FeConnectivity), dimension(ncells) :: c
    integer :: n

    Th = Mesh1d(a, b, ncells)
    fe = LagrangeFe1d(Th)
    do n=1,ncells
       call c(n)%set_values([n, n+1])
    end do
    do n=1,ncells
       call assert_equals(c(n)%values, [n,n+1], 2)
    end do
  end subroutine fe_connectivity_test

end module fe_connectivity_1d_test_module
