module fe_1d_test_module

  use fruit ! Fortran Unit Test Framework
  use nrtypes
  use mesh_1d, only: Mesh1d
  use lagrange_fe_1d, only: LagrangeFe1d
  use lobatto_fe_1d, only: LobattoFe1d
  implicit none

contains

  !!,----------------------------------------------------------------
  !!| Simple test to assert that LagrangeFe1d class is working well
  !!`----------------------------------------------------------------
  subroutine fe_1d_test
    write (*,'(A)',advance="no") "[fe_1d_test/Lagrange]"
    call test_lagrange_ord1_fe1d__A
    call test_lagrange_ord1_fe1d__B
    ! call test_lagrange_ord1_fe_1d__C
    write (*,'(A)',advance="no") "[fe_1d_test/Lobatto]"
    call test_lobatto_ord1_fe1d__B
    call test_lobatto_ord2_fe1d__C
  end subroutine fe_1d_test

  !!,----------------------------------------------------------------------
  !!| Test A: fe construction from a mesh. Reinit of cell.
  !!`----------------------------------------------------------------------
  subroutine test_lagrange_ord1_fe1d__A
    implicit none
    type(Mesh1d), target :: Th
    type(LagrangeFe1d) :: fe
    real(dp), dimension(2) :: x, ref_x
    real(dp), parameter :: a=0.0_dp, b= 2.0_dp
    integer(long) :: i, ncells

    ncells = 3
    Th = Mesh1d(a, b, ncells )
    fe = LagrangeFe1d(Th)

    call assert_equals( fe%order(), 1 )
    call assert_equals( fe%ndof(), 2 )

    do i=1,3
       call fe%reinit(icell=i, order=1)
       associate ( map => fe%affine_map(), h=>(b-a)/ncells )
         ref_x(:) = (/-1._dp, 1._dp /)
         x(:) = (/ (i-1)*h, i*h /)
         ! Test $F$
         call assert_equals( dble(x(:)), dble(map%F(ref_x(:))), 2, delta=1.d-15 )
         ! Test $F^{-1}$ (REMARK: test fails for delta=1.e-15. Round errors?)
         call assert_equals( dble(map%inv_F(x(:))), dble(ref_x(:)), 2, delta=1.d-15 )
       end associate
    end do

  end subroutine test_lagrange_ord1_fe1d__A


  !!,----------------------------------------------------------------------
  !!| Test B: fe construction and affine map
  !!`----------------------------------------------------------------------
  subroutine test_lagrange_ord1_fe1d__B
    implicit none
    type(Mesh1d), target :: Th
    type(LagrangeFe1d) :: fe
    real(dp), dimension(2) :: x, ref_x
    integer(long) :: i

    Th = Mesh1d( 0.0_dp, 1.0_dp, ncells=10 )
    fe = LagrangeFe1d(Th, icell=1)

    do i=1,10
       fe = LagrangeFe1d(Th, icell=i)
       associate ( map => fe%affine_map() )
         ref_x(:) = (/-1._dp, 1._dp /)
         x(:) = (/ (i-1)/10._dp, i/10._dp /)
         ! Test $F$
         call assert_equals( dble(x(:)), dble(map%F(ref_x(:))), 2, delta=1.d-15 )
         ! Test $F^{-1}$ (REMARK: test fails for delta=1.e-15. Round errors?)
         call assert_equals( dble(map%inv_F(x(:))), dble(ref_x(:)), 2, delta=4.d-15 )
       end associate
    end do

  end subroutine test_lagrange_ord1_fe1d__B

  ! !!,----------------------------------------------------------------------
  ! !!| Test C: basis functions
  ! !!`----------------------------------------------------------------------
  ! subroutine test_lagrange_ord1_fe1d__C
  !   implicit none
  !   type(Mesh1d), target :: Th
  !   type(LagrangeFe1d) :: fe
  !   integer(long) :: i
  !   real(dp), dimension(11):: x = [ (i*1.0_dp/10_dp,i=0,10) ] ! Mesh nodes
  !   real(dp), dimension(2) :: ref_x = [-1._dp, 1._dp] ! Reference element nodes
  !   real(dp), dimension(2) :: y0, y1

  !   Th = Mesh1d( 0.0_dp, 1.0_dp, ncells=10 )

  !   ! Assert reference basis functions are OK on vertices {-1,1}
  !   y0 = fe%ref%basis_functs( ref_x(1) ) ! Returns [phi_i0(x_{0}), phi_i1(x_{0})]
  !   y1 = fe%ref%basis_functs( ref_x(2) ) ! Returns [phi_i0(x_{1}), phi_i1(x_{1})]
  !   call assert_equals( dble(y0), dble([1.0, 0.0]), 2, delta=1.d-15)
  !   call assert_equals( dble(y1), dble([0.0, 1.0]), 2, delta=1.d-15)

  !   ! Assert basis function i on node j is the Kronecker $\delta_{i,j}$
  !   do i=1,10
  !      fe = LagrangeFe1d(Th, icell=i)
  !      y0 = fe%basis_functs( x(i) )   ! [phi_i0(x_{i}), phi_i1(x_{i})]
  !      y1 = fe%basis_functs( x(i+1) ) ! [phi_i0(x_{i+1}), phi_i1(x_{i+1})]
  !      call assert_equals( dble(y0), dble([1.0, 0.0]), 2, delta=2.d-15)
  !      call assert_equals( dble(y1), dble([0.0, 1.0]), 2, delta=2.d-15)
  !   end do

  ! end subroutine test_lagrange_ord1_fe1d__C

  !!,----------------------------------------------------------------------
  !!| Test 2.B: fe construction and affine map (Lobatto Hierachic FE)
  !!`----------------------------------------------------------------------
  subroutine test_lobatto_ord1_fe1d__B
    implicit none
    type(Mesh1d), target :: Th
    type(LobattoFe1d) :: fe
    real(dp), dimension(2) :: x, ref_x
    integer(long) :: i

    Th = Mesh1d( 0.0_dp, 1.0_dp, ncells=10 )
    fe = LobattoFe1d(Th, icell=1)

    do i=1,10
       fe = LobattoFe1d(Th, icell=i)
       associate ( map => fe%affine_map() )
         ref_x(:) = (/-1._dp, 1._dp /)
         x(:) = (/ (i-1)/10._dp, i/10._dp /)
         ! Test $F$
         call assert_equals( dble(x(:)), dble(map%F(ref_x(:))), 2, delta=1.d-15 )
         ! Test $F^{-1}$ (REMARK: test fails for delta=1.e-15. Round errors?)
         call assert_equals( dble(map%inv_F(x(:))), dble(ref_x(:)), 2, delta=4.d-15 )
       end associate
    end do

  end subroutine test_lobatto_ord1_fe1d__B

  elemental function lob_phi_2(x) result(y)
    !! Second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = 0.5*sqrt(3._dp/2)*(x**2-1)
  end function lob_phi_2
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  elemental function lob_dx_phi_2(x) result(y)
    !! x-derivative of f second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = sqrt(3._dp/2)*x
  end function lob_dx_phi_2

  !!,----------------------------------------------------------------------
  !!| Test C: basis functions
  !!`----------------------------------------------------------------------
  subroutine test_lobatto_ord2_fe1d__C
    implicit none
    type(Mesh1d), target :: Th
    type(LobattoFe1d) :: fe
    integer(long) :: i
    integer(long), parameter :: order = 2
    real(dp), dimension(0:10):: x = [ (i*1.0_dp/10_dp,i=0,10) ] ! Mesh nodes
    real(dp), dimension(0:10,0:order) :: y

    Th = Mesh1d( 0.0_dp, 1.0_dp, ncells=10 )
    fe = LobattoFe1d(Th)

    call fe%ref_elem%eval_basis_functs(x, output=y)
    call assert_equals(dble(y(:,2)), dble(lob_phi_2(x)), 11, delta=1.d-15)

    call fe%ref_elem%eval_dx_basis_functs(x, output=y)
    call assert_equals(dble(y(:,2)), dble(lob_dx_phi_2(x)), 11, delta=1.d-15)
  end subroutine test_lobatto_ord2_fe1d__C

end module fe_1d_test_module

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! End:
