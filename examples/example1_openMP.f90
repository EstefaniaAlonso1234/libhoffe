program example1_openmp

  use nrtypes, only: dp, long
  use crout_solver, only: solve => solve_tridiagonal_system
  use example1_mod, only:&
       init_lobatto => example1_init_lobatto,&
       build_system => example1_build_system
  ! use omp_lib ! Incorporar funciones para cálculo paralelo con OpenMP

  implicit none

  ! Declaración de variables
  real(dp), allocatable, dimension(:,:) :: A ! Matriz de rigidez
  real(dp), allocatable, dimension(:) :: b ! jmiembro
  real(dp), allocatable, dimension(:) :: u ! Solución del sistema
  integer(long) :: i, ndof
  real :: elapsed1, elapsed2, elapsed3, start, finish ! To store elapsed time

  ! Definición de parámetros
  real(dp), dimension(2), parameter :: interval=[0,1]
  integer(long), parameter :: order=7 ! Grado de los polinomios
  integer(long), parameter :: ncell=2000 ! Número de elementos (celdas)
  real(dp), parameter :: tgv = 1e+30 ! Très grande valeur ;-)
  real(dp), parameter :: bc1 = 0.0, bc2 =0.0 ! Condiciones de contorno
  logical :: resul

  ! Iniciamos elmentos finitos y construimos el sistema de ecuaciones

  call init_lobatto(interval, ncell, order) ! Iniciar el ejemplo1

  ndof = order*ncell+1 ! Número total de grados de libertad
  ! print '("ndof = ", I0 )', ndof
  allocate(A(ndof,ndof)) ! Dimensionar y alojar A en memoria
  allocate(b(ndof), u(ndof)) ! Dimensionar y alojar b y u

  ! call omp_get_wtime(start)
  call cpu_time(start)
  call build_system(A, b) ! Construir el sistema en A, b
  ! call omp_get_wtime(finish)
  call cpu_time(finish)
  elapsed1 = finish-start

  ! Bloqueamos los grados de libertad:

  associate(i_a => 1, i_b => ncell+1) ! Grados de libertad en los extremos del intervalo
    A(i_a,i_a) = tgv
    A(i_b,i_b) = tgv
    b(i_a) = tgv*bc1
    b(i_b) = tgv*bc2
  end associate

  !omp parallel shared(A, b, u) private(i)
  !omp sections
  ! Calcular la solución para los términos de orden 1 (sistema tridiagonal)
  !omp section
  call cpu_time(start)
  resul = solve(A(1:ncell+1,1:ncell+1), b(1:ncell), u(1:ncell)) ! u = solución del sistema de ecuaciones Ax = b
  call cpu_time(finish)
  ! print *, "Section 1"
  elapsed2 = finish-start
  if( resul .neqv. .true. ) then
     stop "Sorry the system can not be solved using crout factorization"
  end if
  ! Calcular la solución para los términos de orden superior (sistema diagonal)
  !omp section
  ! print *, "Section 2"
  call cpu_time(start)
  do i=ncell+2, ndof
     u(i) = b(i)/A(i,i)
  end do
  call cpu_time(finish)
  elapsed3 = finish-start
  !omp end parallel section

  print '("=============================================================================")'
  print '(" Computed solution with polynomial order=",I0," (",I0," cells, ",I0," dofs)")',&
       order, ncell,ndof
  ! print '(" Matrix mounting time: ",G0)', elapsed1
  ! print '(" Solving time (tridiagonal system) : ",G0)', elapsed2
  ! print '(" Solving time (diagonal system) : ",G0)', elapsed3
  print '(" Total solving time : ",G0," seconds")', elapsed2 + elapsed3
  print '("=============================================================================")'


end program example1_openmp
