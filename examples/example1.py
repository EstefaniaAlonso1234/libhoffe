#!/usr/bin/python
# -*- coding: utf-8 -*-
###############################
# Resuelve el problema u''=u0 #
# con u(a)=bc1 y u(b)=bc2     #
###############################
import numpy as np
from numpy.linalg import solve
import pyhoffe
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from numpy import log,sqrt

#definición de funciones a usar

def plot_orders_line(h, orders=[1,2]):
    # Dibujar una línea recta representando los órdenes de error indicados
    n=len(h)
    x=np.array([h[n-1], h[0]])
    styles=[":","--"]
    i=0
    for o in orders:
        print i
        y=x**o # f(x)=x^o será, en escala logarítmica, una recta con 
               # pendiente o
        plt.loglog(x,y,styles[i],color='gray',\
                       lw=3,alpha=0.6,label="Order "+str(o))
        i = i+1

def normaL2_cuadrado(lista_x, lista_y):
    #calcula el cuadrado de la norma L2 para una función sobre una partición
    #argumentos:
    #lista_x[i]=coordenada del punto i-ésimo de la partición
    #lista_y[i]=el valor de la función sobre la coordenada i-ésima
    suma=0
    for i in range(len(lista_y)-1):
        suma=suma+0.5*(lista_x[i+1]-lista_x[i])* \
                    (lista_y[i+1]**2+lista_y[i]**2)
    return suma

def normaL2(lista_x, lista_y):
    #calcula la norma L2 para una función def. sobre un conjunto de puntos
    #argumentos:
    #lista_x[i]=coordenada del punto i-ésimo de la partición
    #lista_y[i]=el valor de la función sobre la coordenada i-ésima
    return sqrt(normaL2_cuadrado(lista_x, lista_y))

def normaL2prima_cuadrado(lista_x, lista_y):
    #calcula el cuadrado de la norma L2 para la derivada de una función
    #argumentos:
    #lista_x[i]=coordenada del punto i-ésimo de la partición
    #lista_y[i]=el valor de la función sobre la coordenada i-ésima
    suma=0
    num=0
    den=0
    for i in range(len(lista_y)-1):
        num=(lista_y[i+1]-lista_y[i])
        den=(lista_x[i+1]-lista_x[i])
        suma=suma+0.5*(lista_x[i+1]-lista_x[i])*(num/den)**2
    return suma

def normaH1(lista_x,lista_y):
    #calcula la norma H1 para una función def. sobre un conjunto de puntos
    #argumentos:
    #lista_x[i]=coordenada del punto i-ésimo de la partición
    #lista_y[i]=el valor de la función sobre la coordenada i-ésima
    return sqrt(normaL2_cuadrado(lista_x,lista_y)+ \
                    normaL2prima_cuadrado(lista_x,lista_y))

def uexacta(x):
    #aquí definimos la solución exacta del problema
    #ha de coincidir con la especificada en el fichero "example1.f90"
    return np.sin( np.pi * x)

interval = [0,1] #intervalo sobre el que hacemos la partición
#creamos la partición
listacells = [2,4,8,16,32,64]
listah = [1.0/n for n in listacells]
#definimos listas
vectorerror = []
vectorerrorL2 = []
vectorerrorH1 = []

order=1 #orden de los polinomios
#para cada celda:
for i in range(len(listah)):
    ncell=listacells[i]
    ndofs = ncell+1

    #llamamos a la biblioteca
    pyhoffe.example1_init_lobatto(interval, ncell, order)
    A, b = pyhoffe.example1_build_system(ncell, order)
    #bloqueo de grados de libertad
    tgv = 1e+30
    bc1, bc2 = 0.0, 0.0 # Condiciones de contorno

    A[0,0] = tgv
    A[-1,-1] = tgv
    b[0] = bc1*tgv
    b[-1] = bc2*tgv
    #resolvemos el sistema de ecuaciones
    u = solve(A,b)

    #partición
    x = np.linspace(0.0, 1.0, ndofs)
    xx = np.linspace(0.0,1.0,200)
    #interpolamos los puntos para hallar la solución aproximada
    uinterp=interp1d(x,u)

    plt.plot(xx, uinterp(xx), "--" ,label="u aprox")
    plt.plot(xx, uexacta(xx), label="u exacta")
    plt.legend()
    plt.title(r"$h=%g$" % (listah[i]))
    plt.show()

    #errores
    error=abs(uexacta(xx)-uinterp(xx))
    errorinf=max(error)
    errorL2=normaL2(xx,error)
    errorH1=normaH1(xx,error)
    vectorerror.append(errorinf)
    vectorerrorL2.append(errorL2)
    vectorerrorH1.append(errorH1)
    print "vectorerror=",vectorerror
    print "vectorerrorL2=",vectorerrorL2
    print "vectorerrorH1=",vectorerrorH1


#grafica loglog
plt.loglog(listah, vectorerror, lw=2, label="Error inf")
plt.loglog(listah, vectorerrorL2, "-o", lw=2, label="Error en L2")
plt.loglog(listah, vectorerrorH1, "-^", lw=2, label="Error en H1")

plot_orders_line( listah, orders=[order,order+1] )

plt.legend(loc="best")
plt.show()

for i in range(len(listah)-1):
    print "i=", i
    p=log(vectorerror[i]/vectorerror[i+1])/log(2)
    print "Orden del método en norma inf es: p=",p
    p=log(vectorerrorL2[i]/vectorerrorL2[i+1])/log(2)
    print "Orden del método en norma L2 es: p=",p
    p=log(vectorerrorH1[i]/vectorerrorH1[i+1])/log(2)
    print "Orden del método en norma H1 es: p=",p
