#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# -----------------------------------------------------------------------
# Resuelve el problema
#   u  - alpha * u'' + beta * u' = u0 en [x_0, x_1]
#   + c.c. Dirichlet en x_0 y x_1
# -----------------------------------------------------------------------
import numpy as np
from numpy.linalg import solve
import pyhoffe
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

# Discretización
interval = [0.,1.]
ncells  = 10
ndof = ncells+1

# Solucion exacta #1
pi=np.pi
sin = np.sin
cos = np.cos
def u_exact(x): return sin(2*pi*x) # Solución exacta
def d1_u_exact(x): return 2*pi*cos(2*pi*x) # Primera derivada
def d2_u_exact(x): return -(4*pi**2)*sin(2*pi*x) # Segunda derivada
uExact = u_exact(np.linspace(interval[0], interval[1], ndof))
d1uExact = d1_u_exact(np.linspace(interval[0], interval[1], ndof))
d2uExact = d2_u_exact(np.linspace(interval[0], interval[1], ndof))

# Solucion exacta #2
L1, C1 = 6*pi, 1
L2, C2 = 2*pi, 3.0
def u_exact(x):
    return C1*sin(L1*x) + C2*sin(L2*x)
def d1_u_exact(x): return C1*L1 * cos(L1*x) + C2*L2 * cos(L2*x)
def d2_u_exact(x): return - C1*L1**2 * sin(L1*x) - C2*L2**2 * sin(L2*x)

uExact = u_exact(np.linspace(interval[0], interval[1], ndof))
d1uExact = d1_u_exact(np.linspace(interval[0], interval[1], ndof))
d2uExact = d2_u_exact(np.linspace(interval[0], interval[1], ndof))

# Sol. exacta #3
m = 10
def u_exact(x): return x**m * np.exp(x)
def d1_u_exact(x): return x**(m-1) * np.exp(x) * (x+m)
def d2_u_exact(x): return x**(m-1) * np.exp(x) * (x**2 + 2*m*x + m**2 -m)

uExact = u_exact(np.linspace(interval[0], interval[1], ndof))
d1uExact = d1_u_exact(np.linspace(interval[0], interval[1], ndof))
d2uExact = d2_u_exact(np.linspace(interval[0], interval[1], ndof))

# Definición del problema

alpha = 1.e-2 # 1.0/(4*pi**2)
beta = 1.e+0 #1.0/(2*pi)
print "alpha, beta =", alpha, beta

u0 = uExact - alpha*d2uExact + beta*d1uExact # = uExact - alpha*uExact''

print "uExact=", uExact
print "u0=", u0

bc1, bc2 = u_exact(interval[0]), u_exact(interval[1]) # Boundary conditions

# Solve in interval the equation u - alpha * u'' = u0
A, b = pyhoffe.example6_build_system(interval, u0, alpha, beta, ncells)
# A no es simétrica. Fortran almacena las matrices por columnas, mientras
# que Python (y C) las almacena por filas
A = A.transpose()

#bloqueo de grados de libertad
tgv = 1e+30 # Très grande valeur
A[0,0] = tgv
A[-1,-1] = tgv
b[0] = bc1*tgv
b[-1] = bc2*tgv

if False:
    print "A=", A
    print "b=", b
u = solve(A,b)
print "u=", u

#dibujar
x = np.linspace(0.0, 1.0, ndof)
xx = np.linspace(0.0, 1.0, 200)
#sol.aprox.
uinterp=interp1d(x,u)
plt.plot(xx, u_exact(xx), lw=2, label="sol. exacta")
plt.plot(xx, uinterp(xx), "--", lw=2, label="sol. aprox.")
plt.legend(loc="best")
plt.grid()
plt.title(r"$u - \alpha u'' + \beta  u' = u_0, \quad \alpha=%g,\ \beta= %g$" % (alpha, beta))
plt.show()
