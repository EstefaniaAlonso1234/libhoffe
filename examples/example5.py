#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# -----------------------------------------------------------------------
# Resuelve el problema
#   u_t  - alpha * u'' + beta * u' = 0 en [x_0, x_1]
#   + c.c. Dirichlet en x_0 y x_1
# -----------------------------------------------------------------------
import numpy as np
from numpy.linalg import solve
import pyhoffe
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

#Discretización en tiempo
t_interval=[0.,1.]
t_ncells=10
t_ndof=t_ncells+1

# Discretización
interval = [0.,1.]
ncells  = 100
ndof = ncells+1

# Condición inicial
x1=0.2
x2=0.3
def cond_inicial(x):
    # Devuelve 2-(x-x1)*(x-x2) si $x\in (x1,x2)$, cero en otro caso
    return ( 2-(x-x1)*(x-x2) ) * (x1<x)*(x<x2)

def dibujar_solucion(x, u, label):
    #definimos una función para dibujar la solución, la utilizaremos dos veces
    xx = np.linspace(0.0, 1.0, 300)
    #sol.aprox.
    uinterp=interp1d(x,u)
    plt.axis([0.0,1.0,-1.09,1.09])
    plt.plot(xx, uinterp(xx), lw=2, label=label)
    plt.ylim([0,2.1]) # Ajustar el eje y al intervalo [0, 2.1]
    plt.legend(loc="best")
    plt.grid()
    plt.title(r"$u_t+\beta  u_x = 0, \quad \ \beta= %g$" % ( beta))
    plt.show()

# Definición del problema

#conciones de contorno
aplicar_cond_contorno_2 = True # ¿Aplicar la segunda condición de contorno?
bc1 = cond_inicial(interval[0])
if aplicar_cond_contorno_2:
    bc2 = cond_inicial(interval[1]) # Boundary conditions

#coeficientes alfa, beta
k = (t_interval[-1] - t_interval[0]) / t_ncells # Incremento de tiempo
if aplicar_cond_contorno_2:
    alpha = 0
    #si hay c. contorno a izquierda, la velocidad ha de ser pequeña
    beta = 4.e-1 * k
else:
    alpha = 0
    #en otro caso, nos podemos permitir una velocidad mayor
    beta = 1.0e+0 * k

# condición inicial
x = np.linspace(interval[0], interval[1], ndof)
u0 = cond_inicial(x)
dibujar_solucion(x, u0, label="Cond. inicial")

# Resolución del problema mediante iteraciones en tiempo
for n in range(t_ndof):

    A, b = pyhoffe.example6_build_system(interval, u0, alpha, beta, ncells)
    # A no es simétrica. Fortran almacena las matrices por columnas, mientras
    # que Python (y C) las almacena por filas
    A = A.transpose()

    #bloqueo de grados de libertad
    tgv = 1e+30
    A[0,0] = tgv
    b[0] = bc1*tgv
    if aplicar_cond_contorno_2:
        A[-1,-1] = tgv
        b[-1] = bc2*tgv

    u = solve(A,b)

    # Dibujar
    t_actual = t_interval[0] + k*n # Instante de tiempo actual
    x = np.linspace(0.0, 1.0, ndof)
    dibujar_solucion(x, u, label="$t="+str(t_actual)+"$")

    u0=u
