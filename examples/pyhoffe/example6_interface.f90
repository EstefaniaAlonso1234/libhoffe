subroutine example6_build_system(interval, u0, alpha, beta, ncells, A, b )
  ! Interface for accessing to example1 from python.
  ! This subroutine shall be compiled by "f2py". For this reason,
  ! it is defined as simple as possible

  use nrtypes, only: dp, long
  use example6_mod, only: ex6 => example6_build_system
  implicit none
  real(dp), dimension(2), intent(in) :: interval
  integer(long), intent(in) :: ncells
  real(dp), intent(in) :: alpha, beta
  real(dp), dimension(ncells+1), intent(in) :: u0
  real(dp), dimension(ncells+1, ncells+1), intent(out) :: A
  real(dp), dimension(ncells+1), intent(out) :: b

  print *, "u0=", u0
  print *, "interval=", interval
  print *, "ncells=", ncells
  print *, "alpha=", alpha

  call ex6(interval, u0, alpha, beta, ncells, A, b)
end subroutine example6_build_system
