module example1_mod
  use nrtypes !! Numeric types
  use mesh_1d, only: Mesh1d !! 1-d mesh
  use quad_rule, only: qr_gauss_2pt, qr_gauss_3pt, qr_gauss_4pt, qr_gauss_5pt, qr_gauss_7pt
  use lobatto_fe_1d, only: LobattoFe1d
  implicit none
  integer, parameter :: lagrange_family = 1
  integer, parameter :: lobatto_family = 2
  integer :: fe_family = lagrange_family
  real(dp), dimension(:,:), allocatable :: A
  real(dp), dimension(:), allocatable :: b
  real(dp), dimension(2) :: interval ! Extrema of interval
  integer(long) :: ncells
  integer(long) :: order
  ! logical, parameter :: block_connectivity = .true. ! Block global matrix (parallel solving)
  type(Mesh1d), target :: Th
  type(LobattoFe1d), target :: fe

contains

  subroutine set_fe_family(family)
    ! Choose fe family to be used
    integer, intent(in) :: family
    fe_family = family
  end subroutine set_fe_family

  function get_fe_family()
    ! Return the fe family being used currently
    integer get_fe_family
    get_fe_family = fe_family
  end function get_fe_family

  function func(x)
    ! Function f(x)=x^2, to be used by as rhs for poisson system
    !   u'' = f
    !   u(0) = 0, u(1) = 0
    use nrtypes, only : pi_dp  ! $\pi$, double precission
    real(dp), intent(in) :: x
    real(dp) func
    func = pi_dp**2 * sin(pi_dp*x)
  end function func

  subroutine example1_init_lobatto(interval_, ncells_, order_)
    real(dp), dimension(2), intent(in) :: interval_ ! Extrema of interval
    integer(long), intent(in) :: ncells_
    integer(long), intent(in) :: order_

    interval = interval_
    ncells = ncells_
    order = order_

    ! 1. Allocate arrays
    if(allocated(A)) deallocate(A)
    if(allocated(b)) deallocate(b)
    associate(N => order*ncells + 1)
      allocate(A(N,N))
      allocate(b(N))
    end associate

    ! 2. Define data
    fe_family = lobatto_family
    Th = Mesh1d(interval(1), interval(2), ncells) !construimos la malla
    fe = LobattoFe1d(Th) !construimos e.f. Lobatto
  end subroutine example1_init_lobatto

  ! subroutine example1(interval, ncells, order)
  !   real(dp), dimension(2), intent(in) :: interval ! Extrema of interval
  !   integer(long), intent(in) :: ncells
  !   integer(long), intent(in) :: order

  !   associate(N => order*ncells + 1)
  !     allocate(A(N,N))
  !     allocate(b(N))
  !   end associate

  !   if(fe_family.eq.lagrange_family) then
  !      print *, ">>> Running example1 for *Lagrange* FE >>>"
  !      call example1_lagrange(interval, ncells, order)
  !   else
  !      print *, ">>> Running example1 for *Lobatto* FE >>>"
  !      call example1_lobatto(interval, ncells, order)
  !   endif
  ! end subroutine example1

  ! subroutine example1_lagrange(interval, ncells, order)
  !   ! Assembles the stiffness matrix, A, and de rhs vector b, for the
  !   ! finite element solution of the following problem:
  !   !
  !   ! u'' = -2  in [x_0, x_1]
  !   ! u(x_0) = u(x_1) = 0
  !   !
  !   ! where interval = [x_0, x_1]
  !   use mesh_1d, only: Mesh1d !! 1-d mesh
  !   use lagrange_fe_1d, only: LagrangeFe1d
  !   use fe_computer_lagrange_1d, only:&
  !        StiffnessMatrixComputer => StiffnessMatrixComputer_Lag,&
  !        RhsVectorComputer => RhsVectorComputer_Lag
  !   use fe_connectivity, only: FeConnectivity

  !   real(dp), dimension(2), intent(in) :: interval
  !   integer(long), intent(in) :: ncells
  !   integer(long), intent(in) :: order

  !   type(Mesh1d), target :: Th
  !   type(LagrangeFe1d), target :: fe
  !   type(StiffnessMatrixComputer) :: stiffness_computer
  !   type(RhsVectorComputer) :: rhs_computer
  !   type(FeConnectivity), dimension(ncells) :: c ! Connectivity
  !   integer(long) :: n, i, j, g_ndof, g_i, g_j

  !   if(order.gt.1) stop "Order too large for Lagrange FE"

  !   ! 1. Define data
  !   Th = Mesh1d(interval(1), interval(2), ncells) !construimos la malla
  !   fe = LagrangeFe1d(Th) !construimos e.f. Lagrange

  !   include "stiffness_inc.f90"
  ! end subroutine example1_lagrange

  function connectivity_for_cell(icell) result(c)
    ! Define connectivity values for a given cell
    use nrtypes, only: long
    integer(long), dimension(order+1) :: c
    integer(long), intent(in) :: icell
    integer(long) :: i
    ! if(order.ne.2) stop("Error, assumed order 2!")
    ! c = [icell, icell+1, icell+ncells+1]
    ! Connectivity for order 1
    c(1:2) = [icell, icell+1]
    ! Extend connectivity for order > 1
    ! c.extend( range(icell+ncells, icell+ncells+order-1) )
    if(order.gt.1) then
       ! if(block_connectivity) then
       !    c(3:)=[ (icell+i, i=2,order) ]
       ! else
       c(3:)=[ ( (ncells+1) + (icell-1)*(order-1) + i, i=1,order-1) ]
    endif
  end function connectivity_for_cell

  subroutine example1_build_system(A_out, b_out)
    ! Assembles the stiffness matrix, A, and de rhs vector b, for the
    ! finite element solution of the following problem:
    !
    ! u'' = -2  in [x_0, x_1]
    ! u(x_0) = u(x_1) = 0
    !
    ! where interval = [x_0, x_1]
    use fe_computer_lobatto_1d, only:&
         StiffnessMatrixComputer => StiffnessMatrixComputer_Lob,&
         RhsVectorComputer => RhsVectorComputer_Lob
    use fe_connectivity, only: FeConnectivity
    real(dp), dimension(ncells*order+1,ncells*order+1), intent(out) :: A_out
    real(dp), dimension(ncells*order+1), intent(out) :: b_out

    type(StiffnessMatrixComputer) :: stiffness_computer
    type(RhsVectorComputer) :: rhs_computer
    type(FeConnectivity), dimension(ncells) :: c ! Connectivity
    integer(long) :: n, i, j, g_ndof, g_i, g_j


    include "stiffness_inc.f90"

    A_out = A
    b_out = b
  end subroutine example1_build_system

  subroutine eval_in_cell(icell, order, x, u_local, y)
    !! Evaluate in the point $x\in R$, located in the cell $icell\in Z$,
    !! the polinomial defined as:
    !!  $$ \sum_{i=0}^{order} u_{local}(i)*phi_i(x) $$
    implicit none
    integer(long), intent(in) :: icell, order
    real(dp), intent(in) :: x
    real(dp), dimension(0:), intent(in) :: u_local
    real(dp), intent(out) :: y
    integer(long) :: i
    real(dp) :: hat_x ! Value of x on reference element

    if (order.ne.size(u_local)-1) stop "eval_in_cell: invalid order or size(u_local)"

    ! print '("eval_in_cell(", I0, "),  x=", G0)', icell, x
    ! print *, "  u_local:"
    ! print *, u_local
    call fe%reinit(icell, order) ! Compute element internal data
    y = 0
    associate(am => fe%affine_map())
      do i=0,order
         hat_x = am%inv_F(x)
         y = y + u_local(i)*fe%ref_elem%basisf_list(i)%f(hat_x)
      end do
    end associate
    ! print '("   y=", G0)', y
  end subroutine eval_in_cell

subroutine eval_on_partition(x, u, N, global_ndofs, y)
  !! Evaluate the polinomial with coefficients u(n,i) on each point of
  !! array x of size N (return y). The array x is suppossed to be
  !! crecent (sorted)
  implicit none
  integer(long), intent(in) :: N, global_ndofs
  real(dp), dimension(N), intent(in) :: x
  real(dp), dimension(global_ndofs), intent(in) :: u
  real(dp), dimension(N), intent(out) :: y
  integer(long), dimension(order+1) :: c
  real(dp), dimension(order+1) :: u_coefficients_on_cell
  integer(long) :: i, jcell, k
  real(dp) :: I_a, I_b, h ! Extrema of interval and partition size

  ! n_points_in_each_cell = (N-1)/ncells
  I_a = interval(1)
  I_b = interval(2)
  h = dble(I_b-I_a)/ncells

  ! if (n_points_in_each_cell*ncells.ne.N-1) &
  !      stop("x[:] debe contener k*ncells+1 puntos, para algún $k$ entero positivo")
  ! print *, "x:"
  ! print *, x
  ! print *, "u:"
  ! print *, u
  ! print *, "order:", order
  ! print *, "n_points_in_each_cell:", n_points_in_each_cell
  jcell = 0
  i = 1
  do while(jcell.le.ncells)
     jcell=jcell+1
     ! print *, "############# jcell=", jcell
      k = (jcell-1)*order+1 ! Position of cell in array u(:)
      c = connectivity_for_cell(jcell)
      u_coefficients_on_cell(:) = u(c(:))
      ! print *, "connectivity =", c
      ! print *, "u_coefficients_on_cell:", u_coefficients_on_cell
      do while(i.le.N)
         ! print '(" jcell=", I0, ", i=", I0, ", N=", I0, ", k=", I0)', jcell, i, N, k
         ! print *, "  jcell data:", "[", I_a+(jcell-1)*h, "<=", x(i), "<=", I_a+jcell*h, "]"
         if((I_a+(jcell-1)*h.le.x(i)).and.(x(i).le.I_a+jcell*h)) then
            ! print *, "  eval i=", i
            call eval_in_cell(jcell, order, x(i), u_coefficients_on_cell, y(i))
            i = i+1
         else
            exit ! Break and go to outer do loop
         endif
      end do
  end do
  jcell = ncells
  k = (jcell-1)*order+1 ! Position of cell in array u(:)
  c = connectivity_for_cell(jcell)
  u_coefficients_on_cell(:) = u(c(:))
  i = size(x)
  call eval_in_cell(jcell, order, x(i), u_coefficients_on_cell, y(i))
  ! do jcell = 1, ncells
  !    k = (jcell-1)*order+1 ! Position of cell in arrays x(:), u(:), y(:)
  !    c = connectivity_for_cell(jcell)
  !    u_coefficients_on_cell(:) = u(c(:))
  !    print *, "----------------------------------------------------------------------"
  !    do i = 0, n_points_in_each_cell
  !       print '(" jcell=", I0, ", iorder=", I0, ", k=", I0)', jcell, i, k
  !       print *, "connectivity =", c
  !       print *, "u_coefficients_on_cell:", u_coefficients_on_cell
  !       ! print *, u(k:k+order)
  !       call eval_in_cell(jcell, order, x(k+i), u_coefficients_on_cell, y(k+i))
  !    end do
  ! end do
end subroutine eval_on_partition

end module example1_mod
