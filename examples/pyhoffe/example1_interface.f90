subroutine example1_init_lobatto(interval, ncells, order)
  ! Interface for accessing to example1 from python.
  ! This subroutine shall be compiled by "f2py". For this reason,
  ! it is defined as simple as possible

  use nrtypes, only: dp, long
  use example1_mod, only: init => example1_init_lobatto
  implicit none
  real(dp), dimension(2), intent(in) :: interval
  integer(long), intent(in) :: ncells
  integer(long), intent(in) :: order

  call init(interval, ncells, order)
end subroutine example1_init_lobatto

subroutine example1_build_system(ncells, order, A_out, b_out)
  use nrtypes, only: dp, long
  use example1_mod, only: build_sys => example1_build_system
  implicit none
  integer(long), intent(in) :: ncells
  integer(long), intent(in) :: order
  real(dp), dimension(ncells*order+1,ncells*order+1), intent(out) :: A_out
  real(dp), dimension(ncells*order+1), intent(out) :: b_out

  call build_sys(A_out, b_out)
end subroutine example1_build_system

subroutine example1_eval_on_partition(x, u, N, global_ndofs, y)
  !! Evaluate the polinomial with coefficients u(i) on
  !! each point of array x of dimension ncells*order+1 (return y)
  use nrtypes, only: dp, long
  use example1_mod, only: eval => eval_on_partition
  implicit none
  integer(long), intent(in) :: N, global_ndofs
  real(dp), dimension(N), intent(out) :: y
  real(dp), dimension(N), intent(in):: x
  real(dp), dimension(global_ndofs), intent(in):: u
   ! real(dp), dimension(ncells,order+1), intent(out) :: y
  ! print *, "N=", N
  ! print *, "shape(u)=", shape(u)

  ! print *, "ok..."
  ! print *, "x:"
  ! print *, x
  ! print *, "u:"
  ! print *, u

  call eval(x, u, N, global_ndofs, y)
  ! print *, "y:"
  ! print *, y
end subroutine example1_eval_on_partition
