  ! 1. Assure that dimension of A and b ar OK
  g_ndof = order*ncells+1  !numero global de grados de libertad
  if(size(a,1) .ne. g_ndof .or. size(a,2) .ne. g_ndof)&
       stop "Size of A must match number of dofs"
  if(size(b).ne.g_ndof) stop "Size of b must match number of dofs"

  ! 2. Define connectivity info:
  ! First lower order (phi_0 and phi_1) dof, then higher order dof
  do n=1, ncells
     call c(n)%set_values(connectivity_for_cell(n))
  end do

  ! 3. Compute global stiffness matrix
  A = 0 ! Assure we start from empty matrix
  stiffness_computer = StiffnessMatrixComputer(fe, quad_rule=qr_gauss_7pt) ! construimos calculadora de m. rigidez
  do n = 1, ncells
     ! print '("Cell ", I0, ":")', n
     ! print *, "Conectivity:"
     ! print *, c(n)%values(:)
     call fe%reinit(icell=n, order=order) ! Compute element internal data
     call stiffness_computer%reinit() ! Idem for stiffness computer
     call stiffness_computer%compute()
     ! print *, "Stifness *matrix* .................."
     ! print *, stiffness_computer%local_matrix
     do i = 1, fe%ndof() ! i=1,2
        g_i = c(n)%values(i) ! Global index for i
        do j = 1, fe%ndof()
           g_j = c(n)%values(j) ! Global index for j
           ! print '("g_i, g_j = ", I0, ", ", I0)', g_i, g_j
           A(g_i, g_j) = A(g_i, g_j) + stiffness_computer%local_matrix(i,j)
        end do
     end do
     ! print *, "End cell"
  end do

  ! 4. Compute rhs vector
  b = 0 ! Assure we start from empty vector
  rhs_computer = RhsVectorComputer(fe, quad_rule=qr_gauss_7pt)
  do n=1,ncells
     ! print '("Cell ", I0, ":")', n
     call fe%reinit(icell=n, order=order)
     call rhs_computer%reinit_function(func)
     call rhs_computer%compute()
     ! print *, "Stifness *vector* .................."
     ! print *,rhs_computer%local_vector
     do i = 1, fe%ndof() ! i=1,2
        g_i = c(n)%values(i) ! Global index for i
        b(g_i) = b(g_i) + rhs_computer%local_vector(i)
     end do
  end do
