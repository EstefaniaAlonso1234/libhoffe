module example6_mod
  use nrtypes !! Numeric types
  implicit none
contains

  function func(x)
    ! Function f(x)=x^2, to be used by as rhs for poisson system
    real(dp), intent(in) :: x
    real(dp) func
    func = 0_dp
  end function func

  subroutine example6_build_system(interval, u0, alpha, beta, ncells, A, b)
    ! Assembles the stiffness matrix, A, and de rhs vector b, for the
    ! finite element solution of the following problem:
    !
    ! u - alpha * u'' + beta * u' = u0 in [x_0, x_1]
    !
    ! NO DIRICHLET CONDITIONS ARE IMPOSSED (The resulting linear
    ! system should be modified in order to assure well-posedness)
    !
    ! args:
    !   interval : array [x_0, x_1]
    !   ncells : number of subdivisions of interval
    !   alpha : real coefficient wich mutiplies the diffusive term
    !   u0 : Vector which gathers right hand side (explicit) terms.
    !        Specifically, u0(i) = sum of all rhs values on node i (i=1,...,ndof).
    use mesh_1d, only: Mesh1d !! 1-d mesh
    use lagrange_fe_1d, only: LagrangeFe1d
    use quad_rule, only: qr_gauss_2pt
    use fe_computer_lagrange_1d, only:&
         MassMatrixComputer => MassMatrixComputer_Lag,&
         StiffnessMatrixComputer => StiffnessMatrixComputer_Lag,&
         TransportMatrixComputer => TransportMatrixComputer_Lag,&
         RhsVectorComputer => RhsVectorComputer_Lag
    use fe_connectivity, only: FeConnectivity

    real(dp), dimension(2), intent(in) :: interval
    integer(long), intent(in) :: ncells
    real(dp), intent(in) :: alpha, beta
    real(dp), dimension(ncells+1), intent(in) :: u0
    real(dp), dimension(ncells+1, ncells+1), intent(out) :: A
    real(dp), dimension(ncells+1), intent(out) :: b

    ! real(dp), dimension(2), parameter :: interval = [0_dp, 1_dp]
    ! real(dp), parameter :: alpha = 9_dp
    type(Mesh1d), target :: Th
    type(LagrangeFe1d), target :: fe
    type(MassMatrixComputer) :: mass_computer
    type(StiffnessMatrixComputer) :: stiffness_computer
    type(TransportMatrixComputer) :: transport_computer
    type(RhsVectorComputer) :: rhs_computer
    real(dp), dimension(:), allocatable, target :: source_v ! Source for rhs computer. ASSUMING 2-PTS QUAD FORM!
    type(FeConnectivity), dimension(ncells) :: c ! Connectivity
    integer(long) :: n, i, j, g_ndof, g_i, g_j

    ! 1. Define data
    g_ndof = ncells+1  !numero global de grados de libertad
    if(size(a,1) .ne. g_ndof .or. size(a,2) .ne. g_ndof)&
         stop "Size of a must match number of dofs"
    if(size(b) .ne. g_ndof) stop "Size of b must match number of dofs"

    Th = Mesh1d(interval(1), interval(2), ncells) !construimos la malla
    fe = LagrangeFe1d(Th) !construimos e.f. Lagrange
    ! Define (trivial) connectivity info:
    do n=1, ncells
       call c(n)%set_values([n,n+1])
    end do

    ! 2. Compute global matrix
    A = 0 ! Assure we start from empty matrix
    mass_computer = MassMatrixComputer(fe, quad_rule=qr_gauss_2pt) ! construimos calculadora de m. masa
    stiffness_computer = StiffnessMatrixComputer(fe, quad_rule=qr_gauss_2pt) ! calculadora de m. rigidez
    transport_computer = TransportMatrixComputer(fe, quad_rule=qr_gauss_2pt) ! idem. matriz transporte
    do n = 1, ncells
       call fe%reinit(icell=n, order=1) ! Compute element internal data
       call mass_computer%reinit() ! Idem for mass matrix computer
       call stiffness_computer%reinit() ! Idem for stiffness computer
       call transport_computer%reinit() ! Idem for transport computer
       call mass_computer%compute()
       call stiffness_computer%compute()
       call transport_computer%compute()
       do i = 1, fe%ndof() ! i=1,2
          g_i = c(n)%values(i) ! Global index for i
          do j = 1, fe%ndof()
             g_j = c(n)%values(j) ! Global index for j
             A(g_i, g_j) = A(g_i, g_j)&
                  + mass_computer%local_matrix(i,j)&
                  + alpha*stiffness_computer%local_matrix(i,j)&
                  + beta*transport_computer%local_matrix(i,j)
          end do
       end do
    end do

    ! 3. Compute rhs vector
    b = 0 ! Assure we start from empty vector
    rhs_computer = RhsVectorComputer(fe, quad_rule=qr_gauss_2pt)
    allocate(source_v(size(rhs_computer%qr%nodes())))
    do n=1,ncells
       call fe%reinit(icell=n, order=1)

       ! Interpolate u0 in quadrature points
       associate(&
         u0_1 => u0(n),&   ! u0 on left node
         u0_2 => u0(n+1),& ! u0 on right node
         qr_nodes => rhs_computer%qr%nodes()&
       )
       if(size(qr_nodes).ne.2) stop "ERROR. Se suponen 2 ptos de cuadratura"
       source_v(1) = u0_1 + 0.5*(u0_2-u0_1)*(qr_nodes(1)+1) ! Interpolation 1
       source_v(2) = u0_1 + 0.5*(u0_2-u0_1)*(qr_nodes(2)+1) ! Interpolation 2

       ! Use these interpolated values for rhs_computer
       call rhs_computer%reinit_vector(source_v)
       call rhs_computer%compute()
       do i = 1, fe%ndof() ! i=1,2
          g_i = c(n)%values(i) ! Global index for i
          b(g_i) = b(g_i) + rhs_computer%local_vector(i)
       end do
     end associate
  end do
end subroutine example6_build_system

end module example6_mod
