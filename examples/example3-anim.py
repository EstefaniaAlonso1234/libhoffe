# -*- coding: utf-8 -*-
import numpy as np
from numpy.linalg import solve
import pyhoffe
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import numpy as np
from matplotlib import animation


#Discretización en tiempo
t_interval=[0.,1.]
t_ncells=10
t_ndof=t_ncells+1
pi=np.pi

interval = [0.,1.]
ncells  = 10
ndof = ncells+1
x = np.linspace(0.0, 1.0, ndof)
# Definición del problema
def u_exact(x): return np.sin(2*pi*x) # Solución exacta
def d2_u_exact(x): return -(4*pi**2)*np.sin(2*pi*x) # Segunda derivada

uExact = u_exact(np.linspace(interval[0], interval[1], ndof))
d2uExact = d2_u_exact(np.linspace(interval[0], interval[1], ndof))
print "uExact=", uExact
alpha = 1.0/(4*np.pi**2)
u0 = uExact - alpha*d2uExact # = uExact - alpha*uExact''
print "u0=", u0
bc1, bc2 = u_exact(interval[0]), u_exact(interval[1])
A, b = pyhoffe.example2_build_system(interval, u0, alpha, ncells)

#bloqueo de grados de libertad
tgv = 1e+30 # Très grande valeur
A[0,0] = tgv
A[-1,-1] = tgv
b[0] = bc1*tgv
b[-1] = bc2*tgv

if False:
    print "A=", A
    print "b=", b
u = solve(A,b)
listau=[u]
xx = np.linspace(0.0, 1.0, 200)
#sol.aprox.
uinterp=interp1d(x,u)
plt.axis([0.0,1.0,-1.0,1.0])
plt.plot(xx, uinterp(xx), lw=2, label="$k="+str(0)+"$")
plt.legend(loc="best")
plt.grid()
plt.show()
listauinterp=[interp1d(x,u)]
#lo hacemos en cada instante de tiempo
for n in range(t_ndof):

    # Solve in interval the equation u - alpha * u'' = u0
    A, b = pyhoffe.example2(interval, listau[n], alpha, ncells)

    #bloqueo de grados de libertad
    tgv = 1e+30 # Très grande valeur
    A[0,0] = tgv
    A[-1,-1] = tgv
    b[0] = bc1*tgv
    b[-1] = bc2*tgv

    if False:
        print "A=", A
        print "b=", b
    u = solve(A,b)
    x = np.linspace(0.0, 1.0, ndof)
    listau.append(u)
    listauinterp.append(interp1d(x,u))
    print "listau=",listau

    #dibujar
    x = np.linspace(0.0, 1.0, ndof)
    xx = np.linspace(0.0, 1.0, 200)
    #sol.aprox.
    uinterp=interp1d(x,u)
    plt.axis([0.0,1.0,-1.0,1.0])
    plt.plot(xx, uinterp(xx), lw=2, label="$k="+str((n+1)*1.0/ndof)+"$")
    plt.legend(loc="best")
    plt.grid()
    plt.show()

# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax = plt.axes(xlim=(0, 1), ylim=(-1, 1))
line, = ax.plot([], [], lw=2)

# initialization function: plot the background of each frame
def init():
    line.set_data([], [])
    return line,

# animation function.  This is called sequentially
def animate(i):
    x = np.linspace(0.0, 1.0, 200)
    y = uinterp(i)
    line.set_data(x, y)
    return line,

# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=200, interval=20, blit=True)

# save the animation as an mp4.  This requires ffmpeg or mencoder to be
# installed.  The extra_args ensure that the x264 codec is used, so that
# the video can be embedded in html5.  You may need to adjust this for
# your system: for more information, see
# http://matplotlib.sourceforge.net/api/animation_api.html
anim.save('basic_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])

plt.show()
