#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np
from numpy.linalg import solve
import pyhoffe
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d # P1 (polygonal) interpolator
from scipy.interpolate import lagrange # Lagrange (order n polynomial) interpolator
from math import log,sqrt


# Elegir familia de EF Lagrange o Lobatto
#pyhoffe.set_lagrange_family()
# pyhoffe.set_lobatto_family()

#============================================================================
# Parámetros principales (dominio, nº elementos, orden de polinomios)
#============================================================================
interval = [0,1]
ncell=10
order=3 # Grados de polinomios, hasta 7.
verbosity=1 # Nivel de verborrea (impresión em pantalla)
#============================================================================

pyhoffe.example1_init_lobatto(interval, ncell, order)
A, b = pyhoffe.example1_build_system(ncell, order)


#bloqueo de grados de libertad

tgv = 1e+30 # Très grande valeur
i_a = 0 # Índice del grado de libertad para el extremo izqdo del intrevalo
i_b = ncell # Índice del grado de libertad para el extremo derecho del intervalo
bc_a, bc_b = 0.0, 0.0 # Boundary conditions
A[i_a, i_a] = tgv
A[i_b, i_b] = tgv
b[i_a] = bc_a*tgv
b[i_b] = bc_b*tgv

np.set_printoptions(precision=2) # Dígitos después del punto decimal
if verbosity>1:
    print "A:\n", A
    print "b:\n", b

# 1) Plot first order terms (corresponding to phi_0 and phi_1 shape functions)
# ---------------------------------------------------------------------------
plot_first_order_terms = False
if plot_first_order_terms:
    print "Plotting first order approximation."
    x = np.linspace(interval[0], interval[1], ncell+1)
    print "x:\n", x
    y = u[0:ncell+1]
    print "y:\n", y
    plt.plot(x, y, "-o", lw=2)


# # 2) Plot all the basis functions (including first and higher order ones)
# # ---------------------------------------------------------------------------
# if order> 1:
#     print "Plotting higher order approximation."

#     # Definir una partición, x, formada por ncell subintervalos, cada uno de
#     # los cuales tiene (order-1) nodos interiores. A continuación, calcular,
#     # un vector, y, resultante de evaluar la solución, u, en cada punto de x.
#     global_ndofs = len(u) # Números de grados de libertad globales para EF Lobatto
#     I_a, I_b = interval # Extremos del intervalo
#     N = 10*ncell+1 # Tamaño de la partición donde evaluar la solución
#     x = np.linspace(I_a, I_b, N)
#     y = pyhoffe.example1_eval_on_partition(x, u, N, global_ndofs)
#     print "x...", x
#     print "y...", y

#     # Dibujar un polinomio de orden "order" en cada subintervalo de la partición x
#     for i in range(ncell):
#         k = i*order # Posición de la celda i en el array
#         print "k", k
#         x_i = x[k:k+order+1] # Nodos en la celda i
#         y_i = y[k:k+order+1] # Valores en la celda _i
#         print "Ptos de interpolación en la celda", i, ": ", (x_i, y_i)
#         yinterp=lagrange(x_i, y_i) # Polinomio de interpolación en la celda i
#         xx = np.linspace(x_i[0], x_i[order]) # Partición fina en la celda i
#         plt.plot (xx, yinterp(xx), color="red", lw=2) # Dibujar el polinomio

#     plt.legend(["Order 1 Lobatto elements", "Order 2 Lobatto elements"], loc="best")

# plt.grid()
# plt.show()

# 3) Plot errors

def plot_orders_line(h, orders=[1,2]):
    # Dibujar una línea recta representando los órdenes de error indicados
    n=len(h)
    x=np.array([h[n-1], h[0]])
    styles=[":","--"]
    i=0
    for o in orders:
        print i
        y=x**o # f(x)=x^o será, en escala logarítmica, una recta con pendiente o
        plt.loglog(x,y,styles[i],color='gray',\
                       lw=3,alpha=0.6,label="Order "+str(o))
        i = i+1

# Normas y soexacta
def normaL2_cuadrado(lista_x, lista_y):
    suma=0
    for i in range(len(lista_y)-1):
        suma=suma+0.5*(lista_x[i+1]-lista_x[i])*(lista_y[i+1]**2+lista_y[i]**2)
    return suma
def normaL2(lista_x, lista_y):
    return sqrt(normaL2_cuadrado(lista_x, lista_y))
def normaL2prima_cuadrado(lista_x, lista_y):
    #calcula la norma L2 para la derivada de una función
    #argumentos:
    #lista_x[i]=coordenada del punto i-ésimo de la partición
    #lista_y[i]=el valor de la función sobre la coordenada i-ésima
    suma=0
    num=0
    den=0
    for i in range(len(lista_y)-1):
        num=(lista_y[i+1]-lista_y[i])
        den=(lista_x[i+1]-lista_x[i])
        suma=suma+0.5*(lista_x[i+1]-lista_x[i])*(num/den)**2
    return suma
# def normaL2prima_cuadrado(lista_x, lista_y):
#     suma=0
#     num=0
#     den=0
#     for i in range(len(lista_y)/2-1):
#         num=lista_y[2*(i+1)]-lista_y[2*i]
#         den=lista_x[2*(i+1)]-lista_x[2*i]
#         yprima_i_cuadrado_por_2h = (num*num)/den # Aproximación de (x[i+1]-x[i-1])*(y'[i])^2
#         suma = suma + yprima_i_cuadrado_por_2h
#     return suma
def normaH1(lista_x,lista_y):
    # return sqrt(normaL2(lista_x,lista_y)+normaL2prima(lista_x,lista_y))
    return sqrt(normaL2_cuadrado(lista_x, lista_y) + normaL2prima_cuadrado(lista_x,lista_y))
def uexacta(x):
    #aquí definimos la solución exacta del problema (que, en la versión actual, ha de
    #coincidir con la especificada en el fichero "example1.f90"
    return np.sin( np.pi * x)

listacells = [2,4,8,16,32,64]
listah = [1.0/n for n in listacells]
vectorerror = []
vectorerrorL2 = []
vectorerrorH1 = []

for i in range(len(listah)):
    ncell = listacells[i]

    pyhoffe.example1_init_lobatto(interval, ncell, order)
    A, b = pyhoffe.example1_build_system(ncell, order)
    #bloqueo de grados de libertad
    tgv = 1e+30 # Très grande valeur
    i_a = 0 # Índice del grado de libertad para el extremo izqdo del intrevalo
    i_b = ncell # Índice del grado de libertad para el extremo derecho del intervalo
    bc_a, bc_b = 0.0, 0.0 # Boundary conditions
    A[i_a, i_a] = tgv
    A[i_b, i_b] = tgv
    b[i_a] = bc_a*tgv
    b[i_b] = bc_b*tgv

    u = solve(A,b)
    # if verbosity>1:
         # print "============= u***", u

    global_ndofs = len(u) # Números de grados de libertad globales para EF Lobatto
    I_a, I_b = interval # Extremos del intervalo
    # Cuidado, si el siguiente parámetro es muy grande se puede colgar el ordenador
    N = 10**3 * ncell+1 # Tamaño de la partición donde evaluar la solución
    x = np.linspace(I_a, I_b, N)
    # Evaluar la solución dada por u en cada punto del array x:
    y = pyhoffe.example1_eval_on_partition(x, u, N, global_ndofs)
    # y = u[0:ncell+1] # First order terms
    # N = len(y)
    # x = np.linspace(I_a, I_b, N)
    # print "============= y :", y


    xx = np.linspace(I_a, I_b, 200)
    #sol.aprox.
    yinterp=interp1d(x,y)

    #dibujar
    # plt.plot(xx,yinterp(xx),c="gold")
    # plt.grid()
    # plt.show()

    #errores
    error=abs(uexacta(xx)-yinterp(xx))
    errorinf=max(error)
    errorL2=normaL2(xx,error)
    errorH1=normaH1(xx,error)
    vectorerror.append(errorinf)
    vectorerrorL2.append(errorL2)
    vectorerrorH1.append(errorH1)
    # print "vectorerror=",vectorerror
    # print "vectorerrorL2=",vectorerrorL2
    # print "vectorerrorH1=",vectorerrorH1


#grafica loglog
plt.loglog(listah, vectorerror, lw=2, label="Error inf")

# Estefanía, he cambiado el tipo de línea (añadiendo, círculos y triángulos)
# porque así se pueden distinguir si se imprime en blanco y negro. Creo que
# es una buena costumbre, pero si no te gusta puedeis quitr el "-o" y "-^".
plt.loglog(listah, vectorerrorL2, "-o", lw=2, label="Error en L2")
plt.loglog(listah, vectorerrorH1, "-^", lw=2, label="Error en H1")

# Dibujar triángulo
# pendiente = order+1
# x0 = 0.2*listah[0]+0.8*listah[-1]
# incremento = 5*(listah[0]-listah[-1])
# x1 = x0*(1 + incremento)
# y0 = 0.01*vectorerrorL2[0]+0.99*vectorerrorL2[-1]
# y1 = y0*(1 + pendiente*incremento)
# plt.loglog([x0, x1, x1, x0], [y0, y0, y1, y0], lw=5, c="gray")
# plt.text(1.05*x1, 0.8*y0+0.2*y1, "$"+str(pendiente)+"$", fontsize=15)
# plt.text(0.6*x0+0.4*x1, 0.5*y0, "$1$", fontsize=15)

plot_orders_line( listah, orders=[order,order+1] )

plt.legend(loc="best")
# plt.show()
plt.savefig('/tmp/example1-order'+str(order)+'.png')

if verbosity>0:
    for i in range(len(listah)-1):
        print "i= %i (h=%f/%f)" % (i, listah[i], listah[i+1])
        p=log(vectorerror[i]/vectorerror[i+1])/log(2)
        print "Orden del método en norma inf es: p=",p
        p=log(vectorerrorL2[i]/vectorerrorL2[i+1])/log(2)
        print "Orden del método en norma L2 es: print()=",p
        p=log(vectorerrorH1[i]/vectorerrorH1[i+1])/log(listah[i]/listah[i+1])
        print "Orden del método en norma H1 es: p=",p
