#!/usr/bin/python
# -*- coding: utf-8 -*-
###########################################
# Resuelve el problema                    #
#   u_t - alpha * u'' = 0 en [x_0, x_1]    #
#   con u(x_0)=bc1 y u(x_1)=bc2           #
###########################################
import numpy as np
from numpy.linalg import solve
import pyhoffe
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

#Discretización en tiempo
t_interval=[0.,1.]
t_ncells=10
t_ndof=t_ncells+1
pi=np.pi
#Discretización en espacio
interval = [0.,1.]
ncells  = 10
ndof = ncells+1
x = np.linspace(0.0, 1.0, ndof)
# Definición del problema
def u_exact(x): return np.sin(2*pi*x) # Solución exacta
def d2_u_exact(x): return -(4*pi**2)*np.sin(2*pi*x) # Segunda derivada

uExact = u_exact(np.linspace(interval[0], interval[1], ndof))
d2uExact = d2_u_exact(np.linspace(interval[0], interval[1], ndof))
print "uExact=", uExact
alpha = 1.0/(4*np.pi**2)
u0 = uExact - alpha*d2uExact
print "u0=", u0
bc1, bc2 = u_exact(interval[0]), u_exact(interval[1])

#lo hacemos en cada instante de tiempo
for n in range(t_ndof+1):

    # Resolución del problema
    A, b = pyhoffe.example2_build_system(interval, u0, alpha, ncells)

    #bloqueo de grados de libertad
    tgv = 1e+30 
    A[0,0] = tgv
    A[-1,-1] = tgv
    b[0] = bc1*tgv
    b[-1] = bc2*tgv

    u = solve(A,b)
    xx = np.linspace(0.0, 1.0, 200)
    #interpolación sol.aprox.
    uinterp=interp1d(x,u)
    # Dibujar
    plt.axis([0.0,1.0,-1.0,1.0])
    plt.plot(xx, uinterp(xx), lw=2, label="$k="+str(n*1.0/ndof)+"$")
    plt.legend(loc="best")
    plt.grid()
    plt.show()
    u0=u
