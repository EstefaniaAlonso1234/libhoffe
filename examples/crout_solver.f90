!Based on:
!  Name      : LU FACTORIZATION FOR TRIDIAGONAL SYSTEM
!  Author    : KAWSER
!  Github    : https://gist.github.com/mkawserm/2b2d4c77b817c8b7307a
!  Blog      : http://blog.kawser.org
!  Created   : 22/05/2014 7:48 AM
!
!Short URL : http://goo.gl/Uu9dNV
!
!Purpose   : We'll solve the system using CROUT Factorization
!            for Tridiagonal Linear Systems
!
!Algorithm Reference : NUMERIACAL ANALYSIS
!                      By RICHARD L. BURDEN
!                      J.DOUGLAS FAIRES
!                      Page No - 408

module crout_solver
  use nrtypes
  implicit none
  private
  public solve_tridiagonal_system

 contains

  logical function solve_tridiagonal_system(m, b, x)
    implicit none
    real(dp), intent(in), dimension(:,:) :: m !coefficient matrix
    real(dp), intent(in), dimension(:) :: b !second hand side
    real(dp), intent(out), dimension(:) :: x !solution of the system

    integer(long)::n
    real(dp), dimension(size(x)) :: z
    real(dp), dimension(size(x),size(x)+1) :: a !augmented matrix
    real(dp), dimension(size(x),size(x)+1) :: l !lower triangual matrix
    real(dp), dimension(size(x),size(x)+1) :: u !upper triangual matrix
    integer(long)::i !loop variables

    n = size(x)
    a(:,1:n) = m
    a(:,n+1) = b

    !solve lz = b
    l(1,1) = a(1,1)
    if ( l(1,1) == 0 ) then
       !can not be solved using crout factorization
       solve_tridiagonal_system = .false.
       return
    end if
    u(1,1) = 1.0
    u(1,2) = a(1,2) / l(1,1)
    z(1) = a(1,n+1) / l(1,1)

    do i=2,n-1
       l(i,i-1) = a(i,i-1)
       l(i,i) = a(i,i) - l(i,i-1) * u(i-1,i)

       if ( l(i,i) == 0 ) then
          !can not be solved using crout factorization
          solve_tridiagonal_system = .false.
          return
       end if

       u(i,i) = 1.0
       u(i,i+1) = a(i,i+1) / l(i,i)
       z(i) = ( a(i,n+1) - l(i,i-1) * z(i-1) ) / l(i,i)
    end do

    u(n,n) = 1.0
    l(n,n-1) = a(n,n-1)
    l(n,n) = a(n,n) - l(n,n-1) * u(n-1,n)
    if ( l(n,n) == 0 ) then
       !can not be solved using crout factorization
       solve_tridiagonal_system = .false.
       return
    end if

    z(n) = ( a(n,n+1) - l(n,n-1) * z(n-1) ) / l(n,n)

    !solve ux = z
    x(n) = z(n)
    do i = n-1,1,-1
       x(i) = z(i) - u(i,i+1) * x(i+1)
    end do

    solve_tridiagonal_system = .true.
    return
  end function solve_tridiagonal_system
end module crout_solver

! program a02
!   use nrtypes
!   use crout_solver, only: solve_tridiagonal_system
!   implicit none

!   integer(long)::n !dimension of the matrix
!   real(dp),allocatable,dimension(:,:)::m !augmented matrix
!   real(dp),allocatable,dimension(:,:)::a !coefficient matrix
!   real(dp),allocatable,dimension(:)::b !second hand term
!   real(dp),allocatable,dimension(:)::x !solution of the system

!   logical::r
!   integer(long)::row, column

!   open(10 , file = "a02.txt") !opening input file
!   open(20 , file = "a02_out.txt") !opening output file

!   read(10,*) n !reading the dimension of the matrix from the file

!   allocate( m(n,n+1), a(n,n), b(n), x(n) ) !allocating memory

!   !reading the augmented matrix
!   read(10,*) (  ( m(row,column) , column=1 , n+1 ) , row=1 , n)
! 100 format(1x,f10.3) !this formatting style is used to format the matrix properly

!   write(20,*) "#-------------------- augmented matrix a|b ----------------------------------------#"
!   do row = 1,n
!      do column = 1,n+1
!         write(20,100,advance='no') m(row,column)
!      end do
!      write(20,*) !move write cursor to new line
!   end do

!   a(:,:) = m(:,1:n)
!   b(:) = m(:,n+1)
!   !calling tridiagonal system solver
!   r = solve_tridiagonal_system(a,b,x)
!   if( r .eqv. .true. ) then
!      write(20,*) "solution x"
!      do row=1,n
!         write(20,100,advance="no") x(row)
!      end do
!      write(20,*) !move write cursor to new line
!   else
!      write(20,*) "sorry the system can not be solved using crout factorization"
!   end if

!   deallocate(a, b, x) !deallocating allocated memory
!   close(10) !closing input file
!   close(20) !closing output file
! end program a02
