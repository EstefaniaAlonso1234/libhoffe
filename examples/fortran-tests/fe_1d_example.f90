program fe_1d_example

  use nrtypes
  use mesh_1d, only: Mesh1d
  use lagrange_fe_1d, only: LagrangeOrd1Fe1d

  implicit none

  type(Mesh1d), target :: Th
  type(LagrangeOrd1Fe1d) :: fe
  real(dp),dimension(2)::x
  real(dp),dimension(2)::ref_x=(/ -1,1 /)
!  real(dp),dimension(4)::x_global
  integer(long)::i,r,ncells
  real(dp), parameter :: a=0.0_dp, b=2.0_dp

  ncells = 3
  Th = mesh1d(a, b, ncells)

  !! 1. Test of affine map (and its inverse)
  do r=1, ncells
     fe = LagrangeOrd1Fe1d(Th, icell=r)

     print '("Cell: r=",I3)', r

     associate ( map => fe%affine_map() )
       x(:) = map%F(ref_x(:)) ! Uso 'x(:)' en vez de 'x' para resaltar que es un array 1d
       do i = 1, 2
          print '("  F(",F6.3,") =",F6.3)', ref_x(i), x(i)
       end do

       ref_x(:) = map%inv_F(x(:))
       do i = 1, 2
          print '("  F^{-1}(",F6.3,") =",F6.3)', x(i), ref_x(i)
       end do
     end associate
  end do
  print*

  ! !! 2. Test of basis functions
  ! x_global=(/ (a+i*(b-a)/ncells,i=0,ncells) /)
  ! do r=1,ncells
  !    print '("Cell: r=",I3)', r
  !    fe = LagrangeOrd1Fe1d(Th, icell=r)

  !    x = fe%basis_functs(x_global(r))
  !    print '("  Value of basis functions in 1st node, x_1 =",F5.2,": [",F5.2,",",F5.2,"]")',&
  !         x_global(r), x(1), x(2)
  !    x = fe%basis_functs(x_global(r+1))
  !    print '("  Value of basis functions in 2nd node, x_2 =",F5.2,": [",F5.2,",",F5.2,"]")',&
  !         x_global(r+1), x(1), x(2)
  ! end do

end program fe_1d_example
