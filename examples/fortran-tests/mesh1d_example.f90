program mesh1d_example

  use nrtypes
  use Mesh_1d, only : Mesh1d
  implicit none

  integer(long) :: i, nc, nv
  type(mesh1d) :: Th
  Th=mesh1d(0.0_dp,1.0_dp,10)

  nc = Th%ncells()
  print *, "Number of cells in Th:", nc
  do i=1, nc
     associate( v=>Th%vertices_of_cell(i) )
      print *, " vertices...",v
     end associate
     print *, "Cell", i, ": vertices...", Th%vertices_of_cell(i)
  end do

  nv = Th%nvertices()
  print *, "Number of vertices in Th:", nv
  do i=1, nv
     associate(v=>Th%coordinates_of_vertex(i))
       print *, "coordinates: ",v
     end associate
     !print *, "Vertex", i, ": coordinates...", Th%coordinates(:,i)
  end do


end program mesh1d_example
