program global

  use nrtypes !! Numeric types
  use mesh_1d, only: Mesh1d !! 1-d mesh
  use lagrange_fe_1d, only: LagrangeOrd1Fe1d
  use fe_computer_lagrange_1d, only:&
       MassMatrixComputer => MassMatrixComputer_LagOrd1,&
       StiffnessMatrixComputer => StiffnessMatrixComputer_LagOrd1

  implicit none
  type(Mesh1d), target :: Th !esta es la malla de elementos finitos
  type(LagrangeOrd1Fe1d), target :: fe ! Elemento finito que usaremos
  type(MassMatrixComputer) :: mass_computer
  type(StiffnessMatrixComputer) :: stiffness_computer
  real(dp), parameter :: a=0.0_dp, b=3.0_dp !definimos los extremos del intervalo
  integer(long), parameter :: ncells = 3 !numero de celdas
!  integer(long), parameter :: n=ncells+1 ! nº de nodos(nº de grados de libertad)
!  real(dp), dimension(n,n) :: matriz!matriz A
!  real(dp), dimension(n) :: vector !vector b
  integer :: i

  Th = Mesh1d(a, b, ncells) !construimos la malla
  fe = LagrangeOrd1Fe1d(Th) !construimos e.f. lagrange

  print "('El numero de grados de libertad es:',i5)",fe%ndof()
  print "('El orden de los polinomios es:',i5)",fe%order()

  !hacer una subrutina(es subrutina porque no devuelve ningun valor) que haga esto:
  mass_computer = MassMatrixComputer(fe) !contruimos calculadora de M. Masa
  do i=1,ncells
     call fe%reinit(i)
     call mass_computer%reinit()
     call mass_computer%compute()
     print "('La matriz de masa local',i2,' es:')",i
     print *,mass_computer%local_matrix
  end do
  !hasta aqui

  !hacer una subrutina(es subrutina porque no devuelve ningun valor) que haga esto:
  stiffness_computer = StiffnessMatrixComputer(fe) !contruimos calculadora de M. Masa
  do i=1,ncells
     call fe%reinit(i)
     call stiffness_computer%reinit()
     call stiffness_computer%compute()
     print "('La matriz de rigidez local',i2,' es:')",i
     print *,stiffness_computer%local_matrix
  end do
  !hasta aqui
end program global
