# Documentation for libHOFFE

Current documentation includes:

- Directory *doxygen*: automatically generated documentation (producing HTML and LaTeX formats). For details, see [doxygen web page](http://www.stack.nl/~dimitri/doxygen/).
- Directory *uml*: UML representation of the abstract data types contained in libHOFFE. They are written in LaTeX with the tikz-uml package.
