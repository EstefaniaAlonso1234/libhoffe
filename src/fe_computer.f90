module fe_computer
  use nrtypes, only: long

  implicit none
  private
  public FeComputer

  !-------------------------------------------------------------------
  type, abstract :: FeComputer
     !! Data type responsible for running FE computations in a concrete element
     private
   contains
     procedure(compute_i), pass(this), deferred :: compute ! Run computer
  end type FeComputer
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  abstract interface
     subroutine compute_i(this)
       !! Run local computing
       use nrtypes, only: long
       import FeComputer
       implicit none
       class(FeComputer), intent(inout) :: this !! Current FeComputer
     end subroutine compute_i
  end interface
  !-------------------------------------------------------------------

contains
end module fe_computer
