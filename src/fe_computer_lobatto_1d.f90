module fe_computer_lobatto_1d
  use nrtypes, only: dp, long
  use fe_traits_1d, only: fe_traits => lobatto_fe_traits
  use quad_rule, only : QuadRule, qr_default
  use mesh_1d, only: Mesh1d
  use lobatto_fe_1d, only: LobattoFe1d
  use fe_computer, only: FeComputer

  implicit none
  private
  public FeMatrixComputer_Lobatto,&
       MassMatrixComputer_Lob,&
       StiffnessMatrixComputer_Lob,&
       RhsVectorComputer_Lob

  !-------------------------------------------------------------------
  type, abstract, extends(FeComputer) :: FeMatrixComputer_Lobatto
     !! A specialization of FeComputer for extending Lobatto FE computing capability
     ! TODO: MAKE PRIVATE
     real(dp), dimension(:,:), allocatable :: local_matrix !! Matrix for assemble local data
     type(LobattoFe1d), pointer :: fe_ptr => null() !! Associated finite element
     class(QuadRule), allocatable :: qr !! Quadrature rule to be applied
     real(dp) :: det_J !! Determinant of Jacobian of affine map
   contains
     procedure, pass(this) :: reinit=>reinit_matrix !! Reset data in currrent element
  end type FeMatrixComputer_Lobatto
  !-------------------------------------------------------------------

  !-------------------------------------------------------------------
  type, abstract, extends(FeComputer) :: FeVectorComputer_Lobatto
     !! A specialization of FeComputer for extending Lobatto FE computing capability
     ! TODO: MAKE PRIVATE
     real(dp), dimension(:), allocatable :: source_v  !! Vector which is used for assembling
     real(dp), dimension(:), allocatable :: local_vector !! Vector where local data is assembled
     type(LobattoFe1d), pointer :: fe_ptr => null() !! Associated finite element
     class(QuadRule), allocatable :: qr !! Quadrature rule to be applied
     real(dp) :: det_J !! Determinant of Jacobian of affine map
   contains
     procedure, pass(this) :: reinit_vector !! Reset from data stored in a vector
     procedure, pass(this) :: reinit_function !! Reset data from a function
  end type FeVectorComputer_Lobatto
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ! Why interface doesn't work?
  ! interface reinit
  !    procedure reinit_vector
  !    procedure reinit_function
  ! end interface reinit
  !-------------------------------------------------------------------

  !-------------------------------------------------------------------
  type, extends(FeMatrixComputer_Lobatto) :: MassMatrixComputer_Lob
     !! A specialization of FeMatrixComputer_Lobatto for computing mass matrix
   contains
     procedure, pass(this) :: compute => compute_mass !! Run assembling of local mass matrix
  end type MassMatrixComputer_Lob
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  interface MassMatrixComputer_Lob ! Map typename to constructor function
     procedure MassMatrixComputer_Lob_Constructor
  end interface MassMatrixComputer_Lob
  !-------------------------------------------------------------------

  !-------------------------------------------------------------------
  type, extends(FeMatrixComputer_Lobatto) :: StiffnessMatrixComputer_Lob
     !! A specialization of FeMatrixComputer_Lobatto for computing stiffnes matrix
   contains
     procedure, pass(this) :: compute => compute_stiffness !! Run assembling of local mass matrix
  end type StiffnessMatrixComputer_Lob
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  interface StiffnessMatrixComputer_Lob
     ! Map typename to constructor function
     ! procedure StiffnessMatrixComputer_Lob_Constructor
     procedure StiffnessMatrixComputer_Lob_Constructor
  end interface StiffnessMatrixComputer_Lob
  !-------------------------------------------------------------------

  !-------------------------------------------------------------------
  type, extends(FeVectorComputer_Lobatto) :: RhsVectorComputer_Lob
     !! A specialization of FeMatrixComputer_Lobatto for computing right hand
     !! side terms (from a vector evaluated on reference quadrature nodes)
   contains
     procedure, pass(this) :: compute => compute_rhs !! Run assembling of local mass matrix
  end type RhsVectorComputer_Lob
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  interface RhsVectorComputer_Lob
     ! Map typename to constructor function
     procedure RhsVectorComputer_Lob_Constructor
  end interface RhsVectorComputer_Lob
  !-------------------------------------------------------------------

contains

  !============================== FeMatrixComputer_Lobatto ======================

  subroutine reinit_matrix(this)
    !! Update internal structures of the FeComputer for a new given element
    class(FeMatrixComputer_Lobatto), intent(inout) :: this !! Current FeComputer

    associate( n => this%fe_ptr%ndof() )
      if(.not.allocated(this%local_matrix)) then
         allocate(this%local_matrix(n,n))
      else
         if( size(this%local_matrix,1) .ne. n ) then
            deallocate(this%local_matrix)
            allocate(this%local_matrix(n,n))
         end if
      end if
      this%local_matrix(:,:) = 0
    end associate
    associate(am => this%fe_ptr%affine_map())
      this%det_J = am%det_J()
    end associate
  end subroutine reinit_matrix

  !============================== FeVectorComputer_Lobatto ======================

  !-------------------------------------------------------------------
  subroutine reinit_vector(this, source_v)
    !! Update internal structures of the FeComputer for a new given element
    class(FeVectorComputer_Lobatto), intent(inout) :: this !! Current FeComputer
    real(dp), dimension(:), pointer, intent(in) :: source_v !! Vector of data, evaluated on reference quadrature nodes

    associate( n => this%fe_ptr%ndof() )

      ! 1. Allocate this%local_vector if needed
      if(.not.allocated(this%local_vector)) then
         allocate(this%local_vector(n))
      else
         if( size(this%local_vector) .ne. n ) then
            deallocate(this%local_vector)
            allocate(this%local_vector(n))
         end if
      end if
      this%local_vector(:) = 0

      ! 2. Allocate this%source_v if needed
      if(.not.allocated(this%source_v)) then
         allocate(this%source_v(n))
      else
         if( size(this%source_v) .ne. n ) then
            deallocate(this%source_v)
            allocate(this%source_v(n))
         end if
      end if
    end associate

    ! 3. Initialize data
    this%source_v = source_v
    associate(am => this%fe_ptr%affine_map())
      this%det_J = am%det_J()
    end associate
  end subroutine reinit_vector

  !-------------------------------------------------------------------
  subroutine reinit_function(this, func)
    !! Update internal structures of the FeComputer for a new given element
    class(FeVectorComputer_Lobatto), intent(inout) :: this !! Current FeComputer
    interface
       function func(x)
         use nrtypes, only: dp
         real(dp), intent(in) :: x
         real(dp) :: func
       end function func
    end interface
    integer :: nqr, i

    nqr = size(this%qr%nodes())

    associate( n => this%fe_ptr%ndof() )
    ! 1. Allocate this%local_vector if needed
      if(.not.allocated(this%local_vector)) then
         allocate(this%local_vector(n))
      else
         if( size(this%local_vector) .ne. n ) then
            deallocate(this%local_vector)
            allocate(this%local_vector(n))
         end if
      end if
      this%local_vector(:) = 0
    end associate

    ! 2. Allocate this%source_v if needed
    if(.not.allocated(this%source_v)) then
       allocate(this%source_v(nqr))
    else
       if( size(this%source_v) .ne. nqr ) then
          deallocate(this%source_v)
          allocate(this%source_v(nqr))
       end if
    end if

    ! 3. Store in this%source_v the values of func on F(quad_points)
    ! (i.e. at "physcal" quadrature points):
    associate( am => this%fe_ptr%affine_map(), qr_nodes => this%qr%nodes() )
      do i = 1,nqr
         this%source_v(i) = func( am%F( qr_nodes(i) ) )
      end do
      ! 4. Define also det_J
      this%det_J = am%det_J()
    end associate

  end subroutine reinit_function
  !============================== Mass Matrix ==============================

  !-------------------------------------------------------------------
  function MassMatrixComputer_Lob_Constructor(fe, quad_rule) result(this)
    !! Default constructor
    type(LobattoFe1d), pointer, intent(in) :: fe !! Underlying mesh
    class(QuadRule), intent(in), optional :: quad_rule
    type(MassMatrixComputer_Lob) :: this !! Current FeComputer
    this%fe_ptr => fe
    if(present(quad_rule)) then
       allocate(this%qr, source=quad_rule)
    else
       allocate(this%qr, source=qr_default)
    end if
  end function MassMatrixComputer_Lob_Constructor

  !-------------------------------------------------------------------
  subroutine compute_mass(this)
    !! Compute local mass matrix
    class(MassMatrixComputer_Lob), intent(inout) :: this !! Current FeComputer
    integer(long) :: nqr ! Number of quadrature points
    real(dp), dimension(:,:), allocatable :: phi ! x-deriv. of reference b.f., evaluated on quadrature nodes
    integer(long) :: i, j, ndof

    ndof = this%fe_ptr%ndof() ! Nb. of degrees of freedom
    nqr= this%qr%n_points ! Nb. of quadrature points
    allocate(phi(nqr,ndof))
    ! Evaluate all the basis functions on each quadrature node
    associate(nodes => this%qr%nodes())
      call this%fe_ptr%ref_elem%eval_basis_functs(nodes(:), output=phi(:,:))
    end associate
    ! Compute integral of product of basis functions phi_i*phi_j
    do j=1,ndof
       do i=1,ndof
          ! Compute integral (Jacobian_of_affine_map * phi_i*phi_j)
          this%local_matrix(i,j) =&
               this%det_J * this%qr%integrate(phi(:,i)*phi(:,j))
       end do
    end do
  end subroutine compute_mass

  !============================== Stiffness Matrix ==============================

  !-------------------------------------------------------------------
  function StiffnessMatrixComputer_Lob_Constructor(fe, quad_rule) result(this)
    !! Default constructor
    implicit none
    type(LobattoFe1d), pointer, intent(in) :: fe !! Underlying mesh
    type(StiffnessMatrixComputer_Lob) :: this !! Current FeComputer
    class(QuadRule), intent(in), optional :: quad_rule !! Quadrature formula
    this%fe_ptr => fe
    if(present(quad_rule)) then
       allocate(this%qr, source=quad_rule)
    else
       allocate(this%qr, source=qr_default)
    end if
  end function StiffnessMatrixComputer_Lob_Constructor

  !-------------------------------------------------------------------
  subroutine compute_stiffness(this)
    !! Compute local stiffness matrix
    class(StiffnessMatrixComputer_Lob), intent(inout) :: this !! Current FeComputer
    integer(long) :: nqr ! Number of quadrature points
    real(dp), dimension(:,:), allocatable :: dx_phi ! x-deriv. of reference b.f., evaluated on quadrature nodes
    integer(long) :: i, j, ndof

    ndof = this%fe_ptr%ndof() ! Nb. of degrees of freedom
    nqr = this%qr%n_points
    allocate(dx_phi(nqr,ndof))
    ! Evaluate all the derivative of basis functions on each quadrature node:
    associate(nodes => this%qr%nodes())
      ! print *, "nodes:", nodes
      call this%fe_ptr%ref_elem%eval_dx_basis_functs(nodes(:), output=dx_phi(:,:))
    end associate
    ! Compute integral of product of basis functions phi_i*phi_j:
    ! print *
    do j=1,ndof
       ! print *, "j=", j, ",   dx_phi(:,j)   =", dx_phi(:,j)
       ! print *, "j=", j, ",   dx_phi(:,j)^2 =", dx_phi(:,j)*dx_phi(:,j)
       do i=1,ndof
          ! Compute integral (Jacobian_of_affine_map * phi_i*phi_j):
          this%local_matrix(i,j) =&
               this%qr%integrate(dx_phi(:,i)*dx_phi(:,j)) / (this%det_J)
          ! print '("i=",I0,", j=", I0, ", matrix(i,j)=", G0)', i, j, this%local_matrix(i,j)
          ! print '("1/(this%det_J) =", G0)', 1./ (this%det_J)
       end do
    end do
  end subroutine compute_stiffness

  !============================== RHS Vector ==============================

  !-------------------------------------------------------------------
  function RhsVectorComputer_Lob_Constructor(fe, quad_rule) result(this)
    !! Default constructor
    implicit none
    type(LobattoFe1d), pointer, intent(in) :: fe !! Underlying mesh
    class(QuadRule), intent(in), optional :: quad_rule !! Quadrature formula
    type(RhsVectorComputer_Lob) :: this !! Current FeComputer
    this%fe_ptr => fe
    if(present(quad_rule)) then
       allocate(this%qr, source=quad_rule)
    else
       allocate(this%qr, source=qr_default)
    end if
  end function RhsVectorComputer_Lob_Constructor

  !-------------------------------------------------------------------
  subroutine compute_rhs(this)
    !! Compute right hand side of FE equations system
    class(RhsVectorComputer_Lob), intent(inout) :: this !! Current FeComputer
    integer(long) :: nqr ! Number of quadrature points
    real(dp), dimension(:,:), allocatable :: phi ! x-deriv. of reference b.f., evaluated on quadrature nodes
    integer(long) :: i, ndof

    ndof = this%fe_ptr%ndof() ! Nb. of degrees of freedom
    nqr = this%qr%n_points
    allocate(phi(nqr,ndof))
    ! Evaluate all the basis functions on each quadrature node
    associate(nodes => this%qr%nodes())
      call this%fe_ptr%ref_elem%eval_basis_functs(nodes(:), output=phi(:,:))
    end associate
    ! Compute integral of product of basis functions phi_i*phi_j:
    do i=1,ndof
       ! Compute integral (Jacobian_of_affine_map * phi_i*phi_j):
       this%local_vector(i) =&
             (this%det_J) * this%qr%integrate(phi(:,i)*this%source_v(:))
    end do
  end subroutine compute_rhs

end module fe_computer_lobatto_1d
