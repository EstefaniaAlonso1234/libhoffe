!> Fortran module containing the definition of Quadrature Rule
!!
!! author Estefanía Alonso Alonso
!! author J. Rafael Rodríguez Galván
!!
!! This program is free software; you can redistribute it and/or
!! modify it under the terms of the GNU General Public License
!! as published by the Free Software Foundation; either version 3
!! of the License, or (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program. If not, see <http:!!www.gnu.org/licenses/>.

module abstract_quad_rule
  !! Reference context on the cell (interval) [-1,1], for finite elements 1d
  use nrtypes, only: dp, long !! Double precision and integer long numeric types
  implicit none

  type, abstract :: QuadRule
     !! Abstract type date for quadrature rules.
     !! Concrete classes, representing actual quadrature rules, are derived
     !! from this abstract class.
     integer(long) :: n_points
   contains
     procedure(get_vector_i), pass(this), deferred :: nodes
     procedure(get_vector_i), pass(this), deferred :: weights
     procedure(integrate_i), pass(this), deferred :: integrate_data
     procedure(integrate_am_i), pass(this), deferred :: integrate_data_am
     generic :: integrate => integrate_data, integrate_data_am
  end type QuadRule

  abstract interface
     pure function get_vector_i(this) result(nodes)
       use nrtypes, only: dp
       import QuadRule
       implicit none
       class(QuadRule), intent(in) :: this
       real(dp), dimension(this%n_points) :: nodes
     end function get_vector_i

     pure function integrate_i(this, data) result(integral)
       use nrtypes, only: dp
       import QuadRule
       implicit none
       class(QuadRule), intent(in) :: this
       real(dp), intent(in) :: data(:)
       real(dp) :: integral
     end function integrate_i

     pure function integrate_am_i(this, data, affine_map) result(integral)
       use nrtypes, only: dp
       use affine_map_1d, only: AffineMap1d
       import QuadRule
       implicit none
       class(QuadRule), intent(in) :: this
       real(dp), intent(in) :: data(:)
       class(AffineMap1d), intent(in) :: affine_map
       real(dp) :: integral
     end function integrate_am_i
  end interface

end module abstract_quad_rule

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! End:
