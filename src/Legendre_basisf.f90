module legendre_basisf

  use nrtypes, only: dp

contains

  !-------------------------------------------------------------------

  pure function phi_0(x) result(y)
    !! First nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    if(x.le.x) continue ! TODO: FIX avoid compiler warning (unused dummy argument)
    y = 1._dp
  end function phi_0
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pure  function dx_phi_0(x) result(y)
    !! x-derivative of first nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    if(x.le.x) continue ! TODO: FIX avoid compiler warning (unused dummy argument)
    y = 0._dp
  end function dx_phi_0

  !-------------------------------------------------------------------

  pure  function phi_1(x) result(y)
    !! Second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = x
  end function phi_1
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pure  function dx_phi_1(x) result(y)
    !! x-derivative of f second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    if(x.le.x) continue ! TODO: FIX avoid compiler warning (unused dummy argument)
    y = 1._dp
  end function dx_phi_1

  !-------------------------------------------------------------------

  pure  function phi_2(x) result(y)
    !! Second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = 3._dp/2_dp * x*x - 0.5_dp
  end function phi_2
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pure  function dx_phi_2(x) result(y)
    !! x-derivative of f second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = 3._dp * x
  end function dx_phi_2

  !-------------------------------------------------------------------

end module legendre_basisf
