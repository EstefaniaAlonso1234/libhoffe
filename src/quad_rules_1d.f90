!> Fortran module containing the definition of Quadrature Rule
!!
!! author Estefanía Alonso Alonso
!! author J. Rafael Rodríguez Galván
!!
!! This program is free software; you can redistribute it and/or
!! modify it under the terms of the GNU General Public License
!! as published by the Free Software Foundation; either version 3
!! of the License, or (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program. If not, see <http:!!www.gnu.org/licenses/>.

          
module quad_rule_OnePoint
  use abstract_quad_rule

  type, extends(QuadRule) :: QuadRule_OnePoint
     !! 1d Quadrature Rule on 2 points
     real(dp), dimension(1) :: nodes_
     real(dp), dimension(1) :: weights_
   contains
     procedure, pass(this) :: nodes
     procedure, pass(this) :: weights
     procedure, pass(this) :: integrate_data => QuadRule_OnePoint_integrate
     procedure, pass(this) :: integrate_data_am => QuadRule_OnePoint_integrate_am
  end type QuadRule_OnePoint

  interface integrate
    procedure QuadRule_OnePoint_integrate
    procedure QuadRule_OnePoint_integrate_am
  end interface integrate

  !-------------------------------------------------------------------

contains

  pure function nodes(this)
    class(QuadRule_OnePoint), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: nodes !! Values (on nodes) to be integrated
    nodes = this%nodes_
  end function nodes

  pure function weights(this)
    class(QuadRule_OnePoint), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: weights !! Values (on nodes) to be integrated
    weights = this%weights_
  end function weights

  pure function QuadRule_OnePoint_integrate(this, data) result(value)
    !! Computes the integral of the vector values, i.e. sum_i(weights_i * values_i)
    class(QuadRule_OnePoint), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    real(dp) :: value !! Resulting approximation of the integral
    value = dot_product(this%weights_, data)
  end function

  pure function QuadRule_OnePoint_integrate_am(this, data, affine_map) result(value)
    !! Computes the integral in the domain defined by the affine map of some
    !! data given in that domain.
    use affine_map_1d, only: AffineMap1d
    class(QuadRule_OnePoint), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    class(AffineMap1d), intent(in) :: affine_map !! Self quadrature rule
    real(dp) :: value !! Resulting approximation of the integral
    value = affine_map%det_J()*dot_product(this%weights_, data)
  end function
end module quad_rule_OnePoint

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! End:
!> Fortran module containing the definition of Quadrature Rule
!!
!! author Estefanía Alonso Alonso
!! author J. Rafael Rodríguez Galván
!!
!! This program is free software; you can redistribute it and/or
!! modify it under the terms of the GNU General Public License
!! as published by the Free Software Foundation; either version 3
!! of the License, or (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program. If not, see <http:!!www.gnu.org/licenses/>.

          
module quad_rule_TwoPoints
  use abstract_quad_rule

  type, extends(QuadRule) :: QuadRule_TwoPoints
     !! 1d Quadrature Rule on 2 points
     real(dp), dimension(2) :: nodes_
     real(dp), dimension(2) :: weights_
   contains
     procedure, pass(this) :: nodes
     procedure, pass(this) :: weights
     procedure, pass(this) :: integrate_data => QuadRule_TwoPoints_integrate
     procedure, pass(this) :: integrate_data_am => QuadRule_TwoPoints_integrate_am
  end type QuadRule_TwoPoints

  interface integrate
    procedure QuadRule_TwoPoints_integrate
    procedure QuadRule_TwoPoints_integrate_am
  end interface integrate

  !-------------------------------------------------------------------

contains

  pure function nodes(this)
    class(QuadRule_TwoPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: nodes !! Values (on nodes) to be integrated
    nodes = this%nodes_
  end function nodes

  pure function weights(this)
    class(QuadRule_TwoPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: weights !! Values (on nodes) to be integrated
    weights = this%weights_
  end function weights

  pure function QuadRule_TwoPoints_integrate(this, data) result(value)
    !! Computes the integral of the vector values, i.e. sum_i(weights_i * values_i)
    class(QuadRule_TwoPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    real(dp) :: value !! Resulting approximation of the integral
    value = dot_product(this%weights_, data)
  end function

  pure function QuadRule_TwoPoints_integrate_am(this, data, affine_map) result(value)
    !! Computes the integral in the domain defined by the affine map of some
    !! data given in that domain.
    use affine_map_1d, only: AffineMap1d
    class(QuadRule_TwoPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    class(AffineMap1d), intent(in) :: affine_map !! Self quadrature rule
    real(dp) :: value !! Resulting approximation of the integral
    value = affine_map%det_J()*dot_product(this%weights_, data)
  end function
end module quad_rule_TwoPoints

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! End:
!> Fortran module containing the definition of Quadrature Rule
!!
!! author Estefanía Alonso Alonso
!! author J. Rafael Rodríguez Galván
!!
!! This program is free software; you can redistribute it and/or
!! modify it under the terms of the GNU General Public License
!! as published by the Free Software Foundation; either version 3
!! of the License, or (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program. If not, see <http:!!www.gnu.org/licenses/>.

          
module quad_rule_ThreePoints
  use abstract_quad_rule

  type, extends(QuadRule) :: QuadRule_ThreePoints
     !! 1d Quadrature Rule on 2 points
     real(dp), dimension(3) :: nodes_
     real(dp), dimension(3) :: weights_
   contains
     procedure, pass(this) :: nodes
     procedure, pass(this) :: weights
     procedure, pass(this) :: integrate_data => QuadRule_ThreePoints_integrate
     procedure, pass(this) :: integrate_data_am => QuadRule_ThreePoints_integrate_am
  end type QuadRule_ThreePoints

  interface integrate
    procedure QuadRule_ThreePoints_integrate
    procedure QuadRule_ThreePoints_integrate_am
  end interface integrate

  !-------------------------------------------------------------------

contains

  pure function nodes(this)
    class(QuadRule_ThreePoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: nodes !! Values (on nodes) to be integrated
    nodes = this%nodes_
  end function nodes

  pure function weights(this)
    class(QuadRule_ThreePoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: weights !! Values (on nodes) to be integrated
    weights = this%weights_
  end function weights

  pure function QuadRule_ThreePoints_integrate(this, data) result(value)
    !! Computes the integral of the vector values, i.e. sum_i(weights_i * values_i)
    class(QuadRule_ThreePoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    real(dp) :: value !! Resulting approximation of the integral
    value = dot_product(this%weights_, data)
  end function

  pure function QuadRule_ThreePoints_integrate_am(this, data, affine_map) result(value)
    !! Computes the integral in the domain defined by the affine map of some
    !! data given in that domain.
    use affine_map_1d, only: AffineMap1d
    class(QuadRule_ThreePoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    class(AffineMap1d), intent(in) :: affine_map !! Self quadrature rule
    real(dp) :: value !! Resulting approximation of the integral
    value = affine_map%det_J()*dot_product(this%weights_, data)
  end function
end module quad_rule_ThreePoints

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! End:
!> Fortran module containing the definition of Quadrature Rule
!!
!! author Estefanía Alonso Alonso
!! author J. Rafael Rodríguez Galván
!!
!! This program is free software; you can redistribute it and/or
!! modify it under the terms of the GNU General Public License
!! as published by the Free Software Foundation; either version 3
!! of the License, or (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program. If not, see <http:!!www.gnu.org/licenses/>.

          
module quad_rule_FourPoints
  use abstract_quad_rule

  type, extends(QuadRule) :: QuadRule_FourPoints
     !! 1d Quadrature Rule on 2 points
     real(dp), dimension(4) :: nodes_
     real(dp), dimension(4) :: weights_
   contains
     procedure, pass(this) :: nodes
     procedure, pass(this) :: weights
     procedure, pass(this) :: integrate_data => QuadRule_FourPoints_integrate
     procedure, pass(this) :: integrate_data_am => QuadRule_FourPoints_integrate_am
  end type QuadRule_FourPoints

  interface integrate
    procedure QuadRule_FourPoints_integrate
    procedure QuadRule_FourPoints_integrate_am
  end interface integrate

  !-------------------------------------------------------------------

contains

  pure function nodes(this)
    class(QuadRule_FourPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: nodes !! Values (on nodes) to be integrated
    nodes = this%nodes_
  end function nodes

  pure function weights(this)
    class(QuadRule_FourPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: weights !! Values (on nodes) to be integrated
    weights = this%weights_
  end function weights

  pure function QuadRule_FourPoints_integrate(this, data) result(value)
    !! Computes the integral of the vector values, i.e. sum_i(weights_i * values_i)
    class(QuadRule_FourPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    real(dp) :: value !! Resulting approximation of the integral
    value = dot_product(this%weights_, data)
  end function

  pure function QuadRule_FourPoints_integrate_am(this, data, affine_map) result(value)
    !! Computes the integral in the domain defined by the affine map of some
    !! data given in that domain.
    use affine_map_1d, only: AffineMap1d
    class(QuadRule_FourPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    class(AffineMap1d), intent(in) :: affine_map !! Self quadrature rule
    real(dp) :: value !! Resulting approximation of the integral
    value = affine_map%det_J()*dot_product(this%weights_, data)
  end function
end module quad_rule_FourPoints

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! End:
!> Fortran module containing the definition of Quadrature Rule
!!
!! author Estefanía Alonso Alonso
!! author J. Rafael Rodríguez Galván
!!
!! This program is free software; you can redistribute it and/or
!! modify it under the terms of the GNU General Public License
!! as published by the Free Software Foundation; either version 3
!! of the License, or (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program. If not, see <http:!!www.gnu.org/licenses/>.

          
module quad_rule_FivePoints
  use abstract_quad_rule

  type, extends(QuadRule) :: QuadRule_FivePoints
     !! 1d Quadrature Rule on 2 points
     real(dp), dimension(5) :: nodes_
     real(dp), dimension(5) :: weights_
   contains
     procedure, pass(this) :: nodes
     procedure, pass(this) :: weights
     procedure, pass(this) :: integrate_data => QuadRule_FivePoints_integrate
     procedure, pass(this) :: integrate_data_am => QuadRule_FivePoints_integrate_am
  end type QuadRule_FivePoints

  interface integrate
    procedure QuadRule_FivePoints_integrate
    procedure QuadRule_FivePoints_integrate_am
  end interface integrate

  !-------------------------------------------------------------------

contains

  pure function nodes(this)
    class(QuadRule_FivePoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: nodes !! Values (on nodes) to be integrated
    nodes = this%nodes_
  end function nodes

  pure function weights(this)
    class(QuadRule_FivePoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: weights !! Values (on nodes) to be integrated
    weights = this%weights_
  end function weights

  pure function QuadRule_FivePoints_integrate(this, data) result(value)
    !! Computes the integral of the vector values, i.e. sum_i(weights_i * values_i)
    class(QuadRule_FivePoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    real(dp) :: value !! Resulting approximation of the integral
    value = dot_product(this%weights_, data)
  end function

  pure function QuadRule_FivePoints_integrate_am(this, data, affine_map) result(value)
    !! Computes the integral in the domain defined by the affine map of some
    !! data given in that domain.
    use affine_map_1d, only: AffineMap1d
    class(QuadRule_FivePoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    class(AffineMap1d), intent(in) :: affine_map !! Self quadrature rule
    real(dp) :: value !! Resulting approximation of the integral
    value = affine_map%det_J()*dot_product(this%weights_, data)
  end function
end module quad_rule_FivePoints

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! End:
!> Fortran module containing the definition of Quadrature Rule
!!
!! author Estefanía Alonso Alonso
!! author J. Rafael Rodríguez Galván
!!
!! This program is free software; you can redistribute it and/or
!! modify it under the terms of the GNU General Public License
!! as published by the Free Software Foundation; either version 3
!! of the License, or (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program. If not, see <http:!!www.gnu.org/licenses/>.

          
module quad_rule_SixPoints
  use abstract_quad_rule

  type, extends(QuadRule) :: QuadRule_SixPoints
     !! 1d Quadrature Rule on 2 points
     real(dp), dimension(6) :: nodes_
     real(dp), dimension(6) :: weights_
   contains
     procedure, pass(this) :: nodes
     procedure, pass(this) :: weights
     procedure, pass(this) :: integrate_data => QuadRule_SixPoints_integrate
     procedure, pass(this) :: integrate_data_am => QuadRule_SixPoints_integrate_am
  end type QuadRule_SixPoints

  interface integrate
    procedure QuadRule_SixPoints_integrate
    procedure QuadRule_SixPoints_integrate_am
  end interface integrate

  !-------------------------------------------------------------------

contains

  pure function nodes(this)
    class(QuadRule_SixPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: nodes !! Values (on nodes) to be integrated
    nodes = this%nodes_
  end function nodes

  pure function weights(this)
    class(QuadRule_SixPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: weights !! Values (on nodes) to be integrated
    weights = this%weights_
  end function weights

  pure function QuadRule_SixPoints_integrate(this, data) result(value)
    !! Computes the integral of the vector values, i.e. sum_i(weights_i * values_i)
    class(QuadRule_SixPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    real(dp) :: value !! Resulting approximation of the integral
    value = dot_product(this%weights_, data)
  end function

  pure function QuadRule_SixPoints_integrate_am(this, data, affine_map) result(value)
    !! Computes the integral in the domain defined by the affine map of some
    !! data given in that domain.
    use affine_map_1d, only: AffineMap1d
    class(QuadRule_SixPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    class(AffineMap1d), intent(in) :: affine_map !! Self quadrature rule
    real(dp) :: value !! Resulting approximation of the integral
    value = affine_map%det_J()*dot_product(this%weights_, data)
  end function
end module quad_rule_SixPoints

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! End:
!> Fortran module containing the definition of Quadrature Rule
!!
!! author Estefanía Alonso Alonso
!! author J. Rafael Rodríguez Galván
!!
!! This program is free software; you can redistribute it and/or
!! modify it under the terms of the GNU General Public License
!! as published by the Free Software Foundation; either version 3
!! of the License, or (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program. If not, see <http:!!www.gnu.org/licenses/>.

          
module quad_rule_SevenPoints
  use abstract_quad_rule

  type, extends(QuadRule) :: QuadRule_SevenPoints
     !! 1d Quadrature Rule on 2 points
     real(dp), dimension(7) :: nodes_
     real(dp), dimension(7) :: weights_
   contains
     procedure, pass(this) :: nodes
     procedure, pass(this) :: weights
     procedure, pass(this) :: integrate_data => QuadRule_SevenPoints_integrate
     procedure, pass(this) :: integrate_data_am => QuadRule_SevenPoints_integrate_am
  end type QuadRule_SevenPoints

  interface integrate
    procedure QuadRule_SevenPoints_integrate
    procedure QuadRule_SevenPoints_integrate_am
  end interface integrate

  !-------------------------------------------------------------------

contains

  pure function nodes(this)
    class(QuadRule_SevenPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: nodes !! Values (on nodes) to be integrated
    nodes = this%nodes_
  end function nodes

  pure function weights(this)
    class(QuadRule_SevenPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: weights !! Values (on nodes) to be integrated
    weights = this%weights_
  end function weights

  pure function QuadRule_SevenPoints_integrate(this, data) result(value)
    !! Computes the integral of the vector values, i.e. sum_i(weights_i * values_i)
    class(QuadRule_SevenPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    real(dp) :: value !! Resulting approximation of the integral
    value = dot_product(this%weights_, data)
  end function

  pure function QuadRule_SevenPoints_integrate_am(this, data, affine_map) result(value)
    !! Computes the integral in the domain defined by the affine map of some
    !! data given in that domain.
    use affine_map_1d, only: AffineMap1d
    class(QuadRule_SevenPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    class(AffineMap1d), intent(in) :: affine_map !! Self quadrature rule
    real(dp) :: value !! Resulting approximation of the integral
    value = affine_map%det_J()*dot_product(this%weights_, data)
  end function
end module quad_rule_SevenPoints

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! End:
!> Fortran module containing the definition of Quadrature Rule
!!
!! author Estefanía Alonso Alonso
!! author J. Rafael Rodríguez Galván
!!
!! This program is free software; you can redistribute it and/or
!! modify it under the terms of the GNU General Public License
!! as published by the Free Software Foundation; either version 3
!! of the License, or (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program. If not, see <http:!!www.gnu.org/licenses/>.

          
module quad_rule_EightPoints
  use abstract_quad_rule

  type, extends(QuadRule) :: QuadRule_EightPoints
     !! 1d Quadrature Rule on 2 points
     real(dp), dimension(8) :: nodes_
     real(dp), dimension(8) :: weights_
   contains
     procedure, pass(this) :: nodes
     procedure, pass(this) :: weights
     procedure, pass(this) :: integrate_data => QuadRule_EightPoints_integrate
     procedure, pass(this) :: integrate_data_am => QuadRule_EightPoints_integrate_am
  end type QuadRule_EightPoints

  interface integrate
    procedure QuadRule_EightPoints_integrate
    procedure QuadRule_EightPoints_integrate_am
  end interface integrate

  !-------------------------------------------------------------------

contains

  pure function nodes(this)
    class(QuadRule_EightPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: nodes !! Values (on nodes) to be integrated
    nodes = this%nodes_
  end function nodes

  pure function weights(this)
    class(QuadRule_EightPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: weights !! Values (on nodes) to be integrated
    weights = this%weights_
  end function weights

  pure function QuadRule_EightPoints_integrate(this, data) result(value)
    !! Computes the integral of the vector values, i.e. sum_i(weights_i * values_i)
    class(QuadRule_EightPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    real(dp) :: value !! Resulting approximation of the integral
    value = dot_product(this%weights_, data)
  end function

  pure function QuadRule_EightPoints_integrate_am(this, data, affine_map) result(value)
    !! Computes the integral in the domain defined by the affine map of some
    !! data given in that domain.
    use affine_map_1d, only: AffineMap1d
    class(QuadRule_EightPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    class(AffineMap1d), intent(in) :: affine_map !! Self quadrature rule
    real(dp) :: value !! Resulting approximation of the integral
    value = affine_map%det_J()*dot_product(this%weights_, data)
  end function
end module quad_rule_EightPoints

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! End:
!> Fortran module containing the definition of Quadrature Rule
!!
!! author Estefanía Alonso Alonso
!! author J. Rafael Rodríguez Galván
!!
!! This program is free software; you can redistribute it and/or
!! modify it under the terms of the GNU General Public License
!! as published by the Free Software Foundation; either version 3
!! of the License, or (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program. If not, see <http:!!www.gnu.org/licenses/>.

          
module quad_rule_NinePoints
  use abstract_quad_rule

  type, extends(QuadRule) :: QuadRule_NinePoints
     !! 1d Quadrature Rule on 2 points
     real(dp), dimension(9) :: nodes_
     real(dp), dimension(9) :: weights_
   contains
     procedure, pass(this) :: nodes
     procedure, pass(this) :: weights
     procedure, pass(this) :: integrate_data => QuadRule_NinePoints_integrate
     procedure, pass(this) :: integrate_data_am => QuadRule_NinePoints_integrate_am
  end type QuadRule_NinePoints

  interface integrate
    procedure QuadRule_NinePoints_integrate
    procedure QuadRule_NinePoints_integrate_am
  end interface integrate

  !-------------------------------------------------------------------

contains

  pure function nodes(this)
    class(QuadRule_NinePoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: nodes !! Values (on nodes) to be integrated
    nodes = this%nodes_
  end function nodes

  pure function weights(this)
    class(QuadRule_NinePoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: weights !! Values (on nodes) to be integrated
    weights = this%weights_
  end function weights

  pure function QuadRule_NinePoints_integrate(this, data) result(value)
    !! Computes the integral of the vector values, i.e. sum_i(weights_i * values_i)
    class(QuadRule_NinePoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    real(dp) :: value !! Resulting approximation of the integral
    value = dot_product(this%weights_, data)
  end function

  pure function QuadRule_NinePoints_integrate_am(this, data, affine_map) result(value)
    !! Computes the integral in the domain defined by the affine map of some
    !! data given in that domain.
    use affine_map_1d, only: AffineMap1d
    class(QuadRule_NinePoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    class(AffineMap1d), intent(in) :: affine_map !! Self quadrature rule
    real(dp) :: value !! Resulting approximation of the integral
    value = affine_map%det_J()*dot_product(this%weights_, data)
  end function
end module quad_rule_NinePoints

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! End:
!> Fortran module containing the definition of Quadrature Rule
!!
!! author Estefanía Alonso Alonso
!! author J. Rafael Rodríguez Galván
!!
!! This program is free software; you can redistribute it and/or
!! modify it under the terms of the GNU General Public License
!! as published by the Free Software Foundation; either version 3
!! of the License, or (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program. If not, see <http:!!www.gnu.org/licenses/>.

          
module quad_rule_TenPoints
  use abstract_quad_rule

  type, extends(QuadRule) :: QuadRule_TenPoints
     !! 1d Quadrature Rule on 2 points
     real(dp), dimension(10) :: nodes_
     real(dp), dimension(10) :: weights_
   contains
     procedure, pass(this) :: nodes
     procedure, pass(this) :: weights
     procedure, pass(this) :: integrate_data => QuadRule_TenPoints_integrate
     procedure, pass(this) :: integrate_data_am => QuadRule_TenPoints_integrate_am
  end type QuadRule_TenPoints

  interface integrate
    procedure QuadRule_TenPoints_integrate
    procedure QuadRule_TenPoints_integrate_am
  end interface integrate

  !-------------------------------------------------------------------

contains

  pure function nodes(this)
    class(QuadRule_TenPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: nodes !! Values (on nodes) to be integrated
    nodes = this%nodes_
  end function nodes

  pure function weights(this)
    class(QuadRule_TenPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(this%n_points) :: weights !! Values (on nodes) to be integrated
    weights = this%weights_
  end function weights

  pure function QuadRule_TenPoints_integrate(this, data) result(value)
    !! Computes the integral of the vector values, i.e. sum_i(weights_i * values_i)
    class(QuadRule_TenPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    real(dp) :: value !! Resulting approximation of the integral
    value = dot_product(this%weights_, data)
  end function

  pure function QuadRule_TenPoints_integrate_am(this, data, affine_map) result(value)
    !! Computes the integral in the domain defined by the affine map of some
    !! data given in that domain.
    use affine_map_1d, only: AffineMap1d
    class(QuadRule_TenPoints), intent(in) :: this !! Self quadrature rule
    real(dp), dimension(:), intent(in) :: data !! Values (on nodes) to be integrated
    class(AffineMap1d), intent(in) :: affine_map !! Self quadrature rule
    real(dp) :: value !! Resulting approximation of the integral
    value = affine_map%det_J()*dot_product(this%weights_, data)
  end function
end module quad_rule_TenPoints

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! End:
