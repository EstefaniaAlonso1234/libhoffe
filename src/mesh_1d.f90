!> Fortran module containing the definition of the mesh data type
!!
!! @author Estefanía Alonso Alonso
!! @author J. Rafael Rodríguez Galván
!!
!! This program is free software; you can redistribute it and/or
!! modify it under the terms of the GNU General Public License
!! as published by the Free Software Foundation; either version 3
!! of the License, or (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program. If not, see <http:!!www.gnu.org/licenses/>.

module mesh_1d
  use nrtypes
  implicit none
  private ! Content of module is private, excepting explicitly public stuff
  public :: Mesh1d

  !> Data type for 1-d meshes
  type Mesh1d
     private

     !> @brief Array which stores the coordinates of each vertex
     !!
     !! Each column, j, stores the coordinates of the vertex whose global
     !! index is equal to j
     real(dp), allocatable :: coordinates_(:)

     integer(long) :: ncells_ !< Number of cells (subintervals)
     integer(long) :: nvertices_in_each_element ! Number of vertices / elements

  contains ! Public by default...
    procedure, pass(this) :: ncells
    procedure, pass(this) :: nvertices
    procedure, nopass :: vertices_of_cell
    procedure, pass(this) :: x_coords_of_vertices
    procedure, pass(this) :: coordinates_of_vertex
  end type Mesh1d

  interface Mesh1d ! Map generic to actual name
     procedure Mesh1dConstructor
  end interface Mesh1d

contains

  !> Constructor for a 1d mesh in an given interval
  function Mesh1dConstructor(x1, x2, ncells) result(Th)
    type(Mesh1d) :: Th                  !< Mesh to be built
    real(dp), intent(in) :: x1          !< Left extreme of the domain (interval [x1,x2])
    real(dp), intent(in) :: x2          !< Right extreme of the domain (interval [x1,x2])
    integer(long), intent(in) :: ncells !< Number of cells (subintervals)

    ! integer(long), parameter :: dimension_of_affine_space = 1
    ! integer(long), parameter :: nvertices_in_each_element = 2 ! Number of vertices / elements
    integer(long) :: nvertices, i
    real(dp) :: h

    Th%nvertices_in_each_element = 2

    ! Define vertices for each cell
    Th%ncells_ = ncells
   ! allocate( Th%vertices_(nvertices_in_each_element, ncells) )
    !do i=1, ncells
     !  Th%vertices_(:,i) = [i,i+1]  ! Define vertices in cell i
    !end do

    ! Define coordinates (1d) for each vertex
    nvertices = ncells+1 ! Total number of vertices in this mesh
    !Th%nvertices_ = nvertices
    h = (x2-x1)/ncells
    allocate( Th%coordinates_(nvertices) )
    Th%coordinates_(:) = [ (x1 + (i-1)*h,  i=1,nvertices) ]
  end function Mesh1dConstructor

  !> Return the number of cells in Th
  pure function ncells(this) result(nb_of_cells)
    class(Mesh1d), intent(in) :: this !< 1d mesh
    integer(long) :: nb_of_cells
    nb_of_cells = this%ncells_
  end function ncells

  !> Return the number of vertices in Th
  pure function nvertices(this) result(nb_of_vertices)
    class(Mesh1d), intent(in) :: this !< 1d mesh
    integer(long) :: nb_of_vertices
    nb_of_vertices = this%ncells_+1
  end function nvertices

  !> Return a copy of the vertices contained in the i-th cell of Th
  pure function vertices_of_cell(icell) result(vertices)
    ! class(Mesh1d), intent(in) :: this !< 1d mesh
    integer(long), intent(in) :: icell
    ! integer(long), dimension(this%nvertices_in_each_element) :: vertices
    integer(long), dimension(2) :: vertices
    vertices = (/icell,icell+1/)
  end function vertices_of_cell

  !> Return a copy of the coordinates of the i-th vertex of Th
  pure function coordinates_of_vertex(this, ivertex) result(coordinates)
    class(Mesh1d), intent(in) :: this !< 1d mesh
    integer(long), intent(in) :: ivertex
    integer(long), parameter :: dimension_of_affine_space = 1
    real(dp), dimension(dimension_of_affine_space) :: coordinates
    coordinates = this%coordinates_(ivertex)
  end function coordinates_of_vertex

  !> Return a copy of the coordinates of the i-th vertex of Th
  pure function x_coords_of_vertices(this, icell) result(x_coords)
    class(Mesh1d), intent(in) :: this !< 1d mesh
    integer(long), intent(in) :: icell !< Cell index
    real(dp), dimension(this%nvertices_in_each_element) :: x_coords !< Coordinates of vertices of cell 'icell'
    integer(long) :: i

    associate( vertices => this%vertices_of_cell(icell) )
      do concurrent(i=1:this%nvertices_in_each_element)
         associate( coord => this%coordinates_of_vertex(vertices(i)) )
           x_coords(i) = coord(1) ! x_i = first coordinate
         end associate
      end do
    end associate
  end function x_coords_of_vertices

end module mesh_1d
