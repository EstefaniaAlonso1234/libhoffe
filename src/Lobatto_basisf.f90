module lobatto_basisf

  use nrtypes, only: dp

contains

  !-------------------------------------------------------------------

  pure  function phi_0(x) result(y)
    !! First nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = 0.5_dp-0.5_dp*x
  end function phi_0
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pure  function dx_phi_0(x) result(y)
    !! x-derivative of first nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    if(x.le.x) continue ! TODO: FIX avoid compiler warning (unused dummy argument)
    y = -0.5_dp
  end function dx_phi_0

  !-------------------------------------------------------------------

  pure  function phi_1(x) result(y)
    !! Second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = 0.5_dp+0.5_dp*x
  end function phi_1
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pure  function dx_phi_1(x) result(y)
    !! x-derivative of f second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    if(x.le.x) continue ! TODO: FIX avoid compiler warning (unused dummy argument)
    y = 0.5_dp
  end function dx_phi_1

  !-------------------------------------------------------------------

  pure  function phi_2(x) result(y)
    !! Second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = 0.5*sqrt(3._dp/2)*(x**2-1)
  end function phi_2
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pure  function dx_phi_2(x) result(y)
    !! x-derivative of f second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = sqrt(3._dp/2)*x
  end function dx_phi_2

  !-------------------------------------------------------------------

  pure  function phi_3(x) result(y)
    !! Second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = 0.5*sqrt(5._dp/2)*(x**2-1)*x
  end function phi_3
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pure  function dx_phi_3(x) result(y)
    !! x-derivative of f second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = 1._dp/2*sqrt(5._dp/2)*(3*x**2-1)
  end function dx_phi_3

  !-------------------------------------------------------------------

  pure  function phi_4(x) result(y)
    !! Second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = sqrt(7._dp/2)/8._dp * (x**2-1)*(5*x**2-1)
  end function phi_4
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pure  function dx_phi_4(x) result(y)
    !! x-derivative of f second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = sqrt(7._dp/8) * (5*x**2-3)*x
  end function dx_phi_4

  !-------------------------------------------------------------------

  pure  function phi_5(x) result(y)
    !! Second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = sqrt(9._dp/2)/8._dp * (x**2-1)*(7*x**2-3)*x
  end function phi_5
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pure  function dx_phi_5(x) result(y)
    !! x-derivative of f second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = sqrt(9._dp/(2**7)) * (3 - 30*x**2 + 35*x**4)
  end function dx_phi_5

  !-------------------------------------------------------------------

  pure  function phi_6(x) result(y)
    !! Second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = sqrt(11._dp/2**9) * (x**2-1) * (21*x**4 - 14*x**2 +1)
  end function phi_6
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pure  function dx_phi_6(x) result(y)
    !! x-derivative of f second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = sqrt(11._dp/2**7) * (63*x**4 - 70*x**2 + 15) * x
  end function dx_phi_6

  !-------------------------------------------------------------------

  pure  function phi_7(x) result(y)
    !! Second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = sqrt(13._dp/2**9) * (x**2-1) * (33*x**4 - 30*x**2 +5) * x
  end function phi_7
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pure  function dx_phi_7(x) result(y)
    !! x-derivative of f second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = sqrt(13._dp/2**9) * (231*x**6 -315*x**4 + 105*x**2 - 5 )
  end function dx_phi_7

end module lobatto_basisf
