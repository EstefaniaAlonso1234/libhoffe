!> Fortran module containing the definition of Quadrature Rule
!!
!! author Estefanía Alonso Alonso
!! author J. Rafael Rodríguez Galván
!!
!! This program is free software; you can redistribute it and/or
!! modify it under the terms of the GNU General Public License
!! as published by the Free Software Foundation; either version 3
!! of the License, or (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program. If not, see <http:!!www.gnu.org/licenses/>.

module quad_rule
  use abstract_quad_rule
  use quad_rule_OnePoint
  use quad_rule_TwoPoints
  use quad_rule_ThreePoints
  use quad_rule_FourPoints
  use quad_rule_FivePoints
  use quad_rule_SixPoints
  use quad_rule_SevenPoints
  use quad_rule_EightPoints
  use quad_rule_NinePoints
  use quad_rule_TenPoints

  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  ! Define concrete quadrature rules
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  type(QuadRule_OnePoint) qr_midpoint !! Midpoint Quadrature Formula in [-1,1]
  parameter( qr_midpoint = QuadRule_OnePoint(&
       n_points=1,&
       nodes_=[0.0_dp], weights_=[2.0_dp]) )

  type(QuadRule_TwoPoints) qr_trapezoid !! Trapezoid Quadrature Formula in [-1,1]
  parameter( qr_trapezoid = QuadRule_TwoPoints(&
       n_points=2,&
       nodes_=[-1.0_dp,1.0_dp], weights_=[1.0_dp,1.0_dp]) )

  type(QuadRule_TwoPoints) qr_gauss_2pt !! Gauss Quadrature Formula in [-1,1]
  parameter( qr_gauss_2pt = QuadRule_TwoPoints(&
       n_points=2,&
       nodes_=[-0.577350269189625764509_dp, 0.577350269189625764509_dp],&
       weights_=[1.0_dp,1.0_dp]) )

  type(QuadRule_ThreePoints) qr_gauss_3pt !! Gauss Quadrature Formula in [-1,1]
  parameter( qr_gauss_3pt = QuadRule_ThreePoints(&
       n_points=3,&
       nodes_=[&
       -0.7745966692414833_dp,&
       0._dp,&
       0.7745966692414832_dp],&
       weights_=[&
       0.5555555555555556_dp,&
       0.8888888888888895_dp,&
       0.5555555555555554_dp] ))

  type(QuadRule_FourPoints) qr_gauss_4pt !! Gauss Quadrature Formula in [-1,1]
  parameter( qr_gauss_4pt = QuadRule_FourPoints(&
       n_points=4,&
       nodes_=[&
       -0.8611363115940527_dp,&
       -0.3399810435848563_dp,&
       0.3399810435848563_dp,&
       0.8611363115940526_dp],&
       weights_=[&
       0.3478548451374547_dp,&
       0.6521451548625466_dp,&
       0.6521451548625458_dp,&
       0.3478548451374541_dp] ))
  type(QuadRule_FivePoints) qr_gauss_5pt !! Gauss Quadrature Formula in [-1,1]
  parameter( qr_gauss_5pt = QuadRule_FivePoints(&
       n_points=5,&
       nodes_=[&
       -0.9061798459386641_dp,&
       -0.5384693101056830_dp,&
       -0.1081853856991421E-15_dp,&
       0.5384693101056831_dp,&
       0.9061798459386639_dp],&
       weights_=[&
       0.2369268850561892_dp,&
       0.4786286704993669_dp,&
       0.5688888888888890_dp,&
       0.4786286704993672_dp,&
       0.2369268850561891_dp] ))

  type(QuadRule_SixPoints) qr_gauss_6pt !! Gauss Quadrature Formula in [-1,1]
  parameter( qr_gauss_6pt = QuadRule_SixPoints(&
       n_points=6,&
       nodes_=[&
       -0.9324695142031522_dp,&
       -0.6612093864662647_dp,&
       -0.2386191860831970_dp,&
       0.2386191860831969_dp,&
       0.6612093864662647_dp,&
       0.9324695142031522_dp],&
       weights_=[&
       0.1713244923791705_dp,&
       0.3607615730481384_dp,&
       0.4679139345726904_dp,&
       0.4679139345726910_dp,&
       0.3607615730481382_dp,&
       0.1713244923791708_dp] ))

  type(QuadRule_SevenPoints) qr_gauss_7pt !! Gauss Quadrature Formula in [-1,1]
  parameter( qr_gauss_7pt = QuadRule_SevenPoints(&
       n_points=7,&
       nodes_=[&
       -0.9491079123427585_dp,&
       -0.7415311855993943_dp,&
       -0.4058451513773971_dp,&
        0.2944352847269754E-15_dp,&
        0.4058451513773971_dp,&
        0.7415311855993943_dp,&
        0.9491079123427584_dp],&
       weights_=[&
       0.1294849661688697_dp,&
       0.2797053914892765_dp,&
       0.3818300505051193_dp,&
       0.4179591836734696_dp,&
       0.3818300505051192_dp,&
       0.2797053914892776_dp,&
       0.1294849661688697_dp] ))

  type(QuadRule_EightPoints) qr_gauss_8pt !! Gauss Quadrature Formula in [-1,1]
  parameter( qr_gauss_8pt = QuadRule_EightPoints(&
       n_points=8,&
       nodes_=[&
       -0.9602898564975365_dp,&
       -0.7966664774136270_dp,&
       -0.5255324099163290_dp,&
       -0.1834346424956498_dp,&
        0.1834346424956496_dp,&
        0.5255324099163292_dp,&
        0.7966664774136268_dp,&
        0.9602898564975364_dp],&
       weights_=[&
       0.1012285362903760_dp,&
       0.2223810344533743_dp,&
       0.3137066458778873_dp,&
       0.3626837833783622_dp,&
       0.3626837833783619_dp,&
       0.3137066458778869_dp,&
       0.2223810344533742_dp,&
       0.1012285362903759_dp] ))

  type(QuadRule_NinePoints) qr_gauss_9pt !! Gauss Quadrature Formula in [-1,1]
  parameter( qr_gauss_9pt = QuadRule_NinePoints(&
       n_points=9,&
       nodes_=[&
       -0.9681602395076260_dp,&
       -0.8360311073266360_dp,&
       -0.6133714327005900_dp,&
       -0.3242534234038094_dp,&
       -0.3649533850434330E-15_dp,&
        0.3242534234038092_dp,&
        0.6133714327005907_dp,&
        0.8360311073266359_dp,&
        0.9681602395076259_dp],&
       weights_=[&
       0.8127438836157462E-01_dp,&
       0.1806481606948576_dp,&
       0.2606106964029357_dp,&
       0.3123470770400033_dp,&
       0.3302393550012602_dp,&
       0.3123470770400024_dp,&
       0.2606106964029365_dp,&
       0.1806481606948582_dp,&
       0.8127438836157423E-01_dp] ))

  type(QuadRule_TenPoints) qr_gauss_10pt !! Gauss Quadrature Formula in [-1,1]
  parameter( qr_gauss_10pt = QuadRule_TenPoints(&
       n_points=10,&
       nodes_=[&
       -0.9739065285171715_dp,&
       -0.8650633666889844_dp,&
       -0.6794095682990244_dp,&
       -0.4333953941292472_dp,&
       -0.1488743389816311_dp,&
        0.1488743389816314_dp,&
        0.4333953941292472_dp,&
        0.6794095682990243_dp,&
        0.8650633666889843_dp,&
        0.9739065285171714_dp],&
       weights_=[&
       0.6667134430868846E-01_dp,&
       0.1494513491505800_dp,&
       0.2190863625159823_dp,&
       0.2692667193099961_dp,&
       0.2955242247147523_dp,&
       0.2955242247147531_dp,&
       0.2692667193099947_dp,&
       0.2190863625159819_dp,&
       0.1494513491505811_dp,&
       0.6667134430868851E-01_dp] ))
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  type(QuadRule_ThreePoints), parameter :: qr_default = qr_gauss_3pt !! Default Quad. Rule

end module quad_rule

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! End:
