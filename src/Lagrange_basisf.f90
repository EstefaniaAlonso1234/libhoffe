module lagrange_basisf

  use nrtypes, only: dp

contains

  !-------------------------------------------------------------------

  pure  function phi_0(x) result(y)
    !! First nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = 0.5-0.5_dp*x
  end function phi_0
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pure  function dx_phi_0(x) result(y)
    !! x-derivative of first nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    if(x.le.x) continue ! TODO: FIX avoid compiler warning (unused dummy argument)
    y = -0.5
  end function dx_phi_0

  !-------------------------------------------------------------------

  pure  function phi_1(x) result(y)
    !! Second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    y = 0.5+0.5_dp*x
  end function phi_1
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pure  function dx_phi_1(x) result(y)
    !! x-derivative of f second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    if(x.le.x) continue ! TODO: FIX avoid compiler warning (unused dummy argument)
    y = 0.5
  end function dx_phi_1

  !-------------------------------------------------------------------

  pure  function phi_2(x) result(y)
    !! Second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    !! TODO: resolve in a more elegant way
    y = log(-x**2) !! FORCE ERROR, THIS FUNCTION IS NOT IMPLEMENTED
  end function phi_2
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pure  function dx_phi_2(x) result(y)
    !! x-derivative of f second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    !! TODO: resolve in a more elegant way
    y = log(-x**2) !! FORCE ERROR, THIS FUNCTION IS NOT IMPLEMENTED
  end function dx_phi_2

  !-------------------------------------------------------------------

  pure  function phi_3(x) result(y)
    !! Second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    !! TODO: resolve in a more elegant way
    y = log(-x**2) !! FORCE ERROR, THIS FUNCTION IS NOT IMPLEMENTED
  end function phi_3
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  pure  function dx_phi_3(x) result(y)
    !! x-derivative of f second nodal basis function
    real(dp), intent(in) :: x
    real(dp) y
    !! TODO: resolve in a more elegant way
    y = log(-x**2) !! FORCE ERROR, THIS FUNCTION IS NOT IMPLEMENTED
  end function dx_phi_3

end module lagrange_basisf
