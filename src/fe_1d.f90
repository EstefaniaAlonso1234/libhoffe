module fe_1d
  use nrtypes, only: dp, long
  use mesh_1d, only: Mesh1d

  implicit none
  private
  public Fe1d

  !-------------------------------------------------------------------
  type, abstract :: Fe1d
     !! Abstract class for 1-d finite elements
  !-------------------------------------------------------------------
   contains
     procedure(get_interface), pass(this), deferred :: ndof
     procedure(get_interface), pass(this), deferred :: order
     procedure(reinit_interface), pass(this), deferred :: reinit
  end type Fe1d
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  abstract interface

     pure function get_interface(this)
       use nrtypes, only: long
       import Fe1d
       implicit none
       class(Fe1d), intent(in) :: this
       integer(long) :: get_interface
     end function get_interface

     pure function evaluator_interface_point(this, x)
       use nrtypes, only: dp
       import Fe1d
       implicit none
       class(Fe1d), intent(in) :: this
       real(dp), intent(in) :: x
       real(dp), allocatable, dimension(:) :: evaluator_interface_point
     end function evaluator_interface_point

     pure function evaluator_interface_vector(this, x)
       use nrtypes, only: dp
       import Fe1d
       implicit none
       class(Fe1d), intent(in) :: this
       real(dp), intent(in) :: x(:)
       real(dp), allocatable, dimension(:,:) :: evaluator_interface_vector
     end function evaluator_interface_vector

     subroutine reinit_interface(this, icell, order)
       use nrtypes, only: long, long
       import Fe1d
       implicit none
       class(Fe1d), intent(inout) :: this
       integer(long), intent(in) :: icell
       integer(long), intent(in) :: order
     end subroutine reinit_interface

  end interface
  !-------------------------------------------------------------------

contains

end module fe_1d
