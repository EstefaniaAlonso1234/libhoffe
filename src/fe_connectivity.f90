module fe_connectivity
  use nrtypes, only: long !! Numeric types
  implicit none
  !-------------------------------------------------------------------
  type FeConnectivity
     !! Class for local to global node connectivity for a specific element
     ! TODO: Make private the following array (define get_values()) ???
     integer(long), dimension(:), allocatable :: values !! Connectivity values
   contains
     procedure, pass(this) :: set_values ! Set connectivity values
  end type FeConnectivity

contains

  subroutine set_values(this, v)
    class(FeConnectivity) :: this
    integer(long), dimension(:), intent(in) :: v
    integer(long) :: n
    n = size(v)
    if(.not.allocated(this%values)) then
       allocate(this%values(n))
    else
       if(size(this%values).ne.n) then
          deallocate(this%values)
          allocate(this%values(n))
       endif
    endif
    this%values = v
  end subroutine set_values

end module fe_connectivity
