module affine_map_1d
  use nrtypes
  implicit none
  private
  public AffineMap1d

  !-------------------------------------------------------------------
  type AffineMap1d
  !-------------------------------------------------------------------
     private
     real(dp) :: a, b ! Affine map: ax+b
   contains
     procedure, pass(this) :: F
     procedure, pass(this) :: inv_F
     procedure, pass(this) :: det_J
  end type AffineMap1d
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  interface AffineMap1d ! Map typename to constructor function
     procedure AffineMap1dConstructorCoefficents
     procedure AffineMap1dConstructorIntervals
  end interface AffineMap1d
  !-------------------------------------------------------------------

contains

  !-------------------------------------------------------------------
  ! Constructors
  !-------------------------------------------------------------------
  function AffineMap1dConstructorCoefficents(a, b) result(this)
    !! Construct affine map (ax+b) from its coefficents, a, b
    type(AffineMap1d) :: this
    real(dp), intent(in) :: a, b ! Affine map: ax+b
    this%a = a
    this%b = b
  end function AffineMap1dConstructorCoefficents

  function AffineMap1dConstructorIntervals(ref_I, I) result(this)
    !! Construct affine map F: ref_I -> I from intervals (ref_I, I)
    type(AffineMap1d) :: this
    real(dp), dimension(2), intent(in) :: ref_I, I ! Affine map: ax+b
    real(dp) :: a, b

    a = (I(2)-I(1)) / (ref_I(2) - ref_I(1))
    b = I(1) - a*ref_I(1)
    this%a = a
    this%b = b
  end function AffineMap1dConstructorIntervals

  !-------------------------------------------------------------------
  pure elemental function F(this,x)
  !-------------------------------------------------------------------
    class(AffineMap1d), intent(in) :: this
    real(dp), intent(in) :: x
    real(dp) :: F
    F = this%a * x + this%b
  end function F

  !-------------------------------------------------------------------
  pure elemental function inv_F(this,y) result(x)
  !-------------------------------------------------------------------
    class(AffineMap1d), intent(in) :: this
    real(dp), intent(in) :: y
    real(dp) :: x
    x = (y - this%b)/this%a
  end function inv_F

  !-------------------------------------------------------------------
  ! Returns the determinant of the Jacobian of the affine map
  !-------------------------------------------------------------------
  pure function det_J(this)
    class(AffineMap1d), intent(in) :: this
    real(dp) :: det_J
    det_J = this%a
  end function det_J

end module affine_map_1d
