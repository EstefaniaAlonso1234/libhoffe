module lagrange_fe_1d
  use nrtypes, only: dp, long
  use mesh_1d, only: Mesh1d
  use fe_1d, only: Fe1d
  use fe_traits_1d, only: fe_traits => lagrange_fe_traits
  use affine_map_1d, only: AffineMap1d
  use Lagrange_ref_elem_1d, only: RefElem_Lagrange, the_ref_elem

  implicit none
  private
  public LagrangeFe1d

  !-------------------------------------------------------------------
  type, extends(Fe1d) :: LagrangeFe1d
  !-------------------------------------------------------------------
     private
     type(Mesh1d), pointer :: mesh_ptr_ => null()
     integer(long) :: icell_
     integer(long) :: order_
     type(AffineMap1d) :: affine_map_
     type(RefElem_Lagrange), public, pointer :: ref_elem
   contains
     procedure, pass(this) :: ndof !! Number of degrees of freedom
     procedure, pass(this) :: order !! Order of polynomials
     procedure, pass(this) :: reinit !! Assing a new cell to current element and recalculate data
     procedure, pass(this) :: affine_map !! Get affine map from reference element to self element
  end type LagrangeFe1d
  !- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  interface LagrangeFe1d ! Map typename to constructor function
     procedure LagrangeFe1dConstructor
     procedure LagrangeFe1dConstructorIcell
  end interface LagrangeFe1d
  !-------------------------------------------------------------------

contains

  !-------------------------------------------------------------------
  function LagrangeFe1dConstructor(mesh) result(this)
    !! Constructor from mesh
    implicit none
    type(LagrangeFe1d) :: this
    type(Mesh1d), pointer, intent(in) :: mesh
    integer(long), parameter :: default_order=1

    this%mesh_ptr_ => mesh
    this%icell_ = 0 ! Unassigned cell index
    this%order_ = default_order
    call the_ref_elem%reinit()
    this%ref_elem => the_ref_elem
  end function LagrangeFe1dConstructor

  !-------------------------------------------------------------------
  function LagrangeFe1dConstructorIcell(mesh, icell) result(this)
    !! Constructor from mesh and cell index
    implicit none
    type(LagrangeFe1d) :: this
    type(Mesh1d), pointer, intent(in) :: mesh
    integer(long), intent(in) :: icell
    integer(long), parameter :: default_order=1

    this%mesh_ptr_ => mesh
    call the_ref_elem%reinit()
    call this%reinit(icell, default_order)
  end function LagrangeFe1dConstructorIcell

  !-------------------------------------------------------------------
  subroutine reinit(this, icell, order)
    !! Assing a new cell to current element and recalculate data
    class(LagrangeFe1d), intent(inout) :: this !! Current FE
    integer(long), intent(in) :: icell !! Index of cell for current element
    integer(long), intent(in) :: order !! Index of cell for current element

    this%icell_ = icell
    this%order_ = order
    this%ref_elem => the_ref_elem

    ! TODO: Assert (delete the following line in non devel versions) !!
    if(order.gt.fe_traits%max_order) stop "LagrangeFe1d: max_order exceeded"

    ! Construct a new affine map
    associate( interval => this%mesh_ptr_%x_coords_of_vertices(icell) )
      this%affine_map_ = AffineMap1d(this%ref_elem%ref_cell%vertices, interval)
    end associate
  end subroutine reinit

  !-------------------------------------------------------------------
  pure function affine_map(this)
    class(LagrangeFe1d), intent(in) :: this
    type(AffineMap1d) :: affine_map
    affine_map = this%affine_map_
  end function affine_map

  !-------------------------------------------------------------------
  pure function ndof(this)
    class(LagrangeFe1d), intent(in) :: this
    integer(long) :: ndof
    ndof = this%order_ + 1
  end function ndof

  !-------------------------------------------------------------------
  pure function order(this)
    class(LagrangeFe1d), intent(in) :: this
    integer(long) :: order
    order = this%order_
  end function order

end module lagrange_fe_1d
