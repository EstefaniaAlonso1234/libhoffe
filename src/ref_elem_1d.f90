!> Fortran module containing the definition of reference elements
!!
!! author Estefanía Alonso Alonso
!! author J. Rafael Rodríguez Galván
!!
!! This program is free software; you can redistribute it and/or
!! modify it under the terms of the GNU General Public License
!! as published by the Free Software Foundation; either version 3
!! of the License, or (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program. If not, see <http:!!www.gnu.org/licenses/>.


module Lagrange_ref_elem_1d
  !! Reference context on the cell (interval) [-1,1], for finite elements 1d
  use nrtypes, only: dp, long !! Double precision and int long numeric types
  use fe_traits_1d, only: RefCell, fe_traits => Lagrange_fe_traits
  use Lagrange_basisf
  implicit none
  private
  public&
       RefElem_Lagrange, &
       the_ref_elem

  abstract interface
    pure function basis_f(x)
      use nrtypes, only: dp
      real(dp) :: basis_f
      real(dp), intent(in) :: x
    end function
  end interface
  type BasisfWrapper
     procedure(basis_f), nopass, pointer :: f
     procedure(basis_f), nopass, pointer :: dx_f
  end type BasisfWrapper

  type RefElem_Lagrange
     !! Lagrange 1d Reference Element (in cell [-1,1])
     type(RefCell) :: ref_cell !! Reference cell
     integer(long) :: max_order
     type(BasisfWrapper), dimension(0:fe_traits%max_order) :: basisf_list
   contains
     procedure, pass(this) :: reinit
     procedure, pass(this) :: eval_basis_functs !! Evaluate the basis functions on a point
     procedure, pass(this) :: eval_dx_basis_functs !! Evaluate x-derivative of b.f. on point
  end type RefElem_Lagrange
  ! interface RefElem_Lagrange
  !    procedure RefElem_Lagrange_ctor
  ! end interface RefElem_Lagrange

  !! Global reference element. Finite elements shall store a pointer to it.
  type(RefElem_Lagrange), target :: the_ref_elem

  ! type(RefElem_Lagrange), parameter, dimension(0:1)& !fe_traits%max_order)&
  !      :: ref_elem_list = [ref_elem, ref_elem] !! List of ref elements

  ! type(BasisfWrapper), dimension(0:fe_traits%max_order) :: basisf_list

  ! = [BasisfWrapper(p1), BasisfWrapper(p1)]
  ! interface
  !    function intrinsic init_basisf_list()
  !      logical init_basisf_list
  !    end function init_basisf_list
  ! end interface
  ! logical, parameter :: basisf_list_initialized = init_basisf_list()

contains

  !! Constructor for BasisFunctionContainer
  ! function BasisfWrapper_ctor(p) result(this)
  !   type(BasisfWrapper) :: this
  !   procedure(basis_f), pointer :: p
  !   this%f => p
  ! end function BasisfWrapper_ctor

  subroutine reinit(this)
    class(RefElem_Lagrange) :: this
    this%ref_cell = fe_traits%ref_cell
    this%max_order = fe_traits%max_order
    if (fe_traits%max_order.ne.1) stop&
         "Error, fe_traits%max_order ha de coincidir con (nº basis functions)-1"
    this%basisf_list(0)%f => phi_0
    this%basisf_list(0)%dx_f => dx_phi_0
    this%basisf_list(1)%f => phi_1
    this%basisf_list(1)%dx_f => dx_phi_1
    ! this%basisf_list(2)%f => phi_2
    ! this%basisf_list(2)%dx_f => dx_phi_2
    ! this%basisf_list(3)%f => phi_3
    ! this%basisf_list(3)%dx_f => dx_phi_3
  end subroutine reinit

  !-------------------------------------------------------------------
  subroutine eval_basis_functs(this, x, output)
    !! Return a 2-dimensional vector where the i-th element (i=1,2)
    !! stores the value of the i-th basis function on a point, x, in [-1,1]
    class(RefElem_Lagrange) :: this
    real(dp), dimension(:), intent(in) :: x
    real(dp), dimension(:,:), intent(out) :: output
    integer(long) :: i,j
    do j = 1, size(output,2)
       do i = 1, size(output,1)
          output(i,j) = this%basisf_list(j-1)%f(x(i))
       end do
    end do
  end subroutine eval_basis_functs

  !-------------------------------------------------------------------
  subroutine eval_dx_basis_functs(this, x, output)
    !! Return a 2-dimensional vector where the i-th element (i=1,2)
    !! stores the value of the i-th basis function on a point, x, in [-1,1]
    class(RefElem_Lagrange) :: this
    real(dp), dimension(:), intent(in) :: x
    real(dp), dimension(:,:), intent(out) :: output
    integer(long) :: i,j
    do j = 1, size(output,2)
       do i = 1, size(output,1)
          output(i,j) = this%basisf_list(j-1)%dx_f(x(i))
       end do
    end do
  end subroutine eval_dx_basis_functs
end module Lagrange_ref_elem_1d

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! End:
!> Fortran module containing the definition of reference elements
!!
!! author Estefanía Alonso Alonso
!! author J. Rafael Rodríguez Galván
!!
!! This program is free software; you can redistribute it and/or
!! modify it under the terms of the GNU General Public License
!! as published by the Free Software Foundation; either version 3
!! of the License, or (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program. If not, see <http:!!www.gnu.org/licenses/>.


module Lobatto_ref_elem_1d
  !! Reference context on the cell (interval) [-1,1], for finite elements 1d
  use nrtypes, only: dp, long !! Double precision and int long numeric types
  use fe_traits_1d, only: RefCell, fe_traits => Lobatto_fe_traits
  use Lobatto_basisf
  implicit none
  private
  public&
       RefElem_Lobatto, &
       the_ref_elem

  abstract interface
    pure function basis_f(x)
      use nrtypes, only: dp
      real(dp) :: basis_f
      real(dp), intent(in) :: x
    end function
  end interface
  type BasisfWrapper
     procedure(basis_f), nopass, pointer :: f
     procedure(basis_f), nopass, pointer :: dx_f
  end type BasisfWrapper

  type RefElem_Lobatto
     !! Lagrange 1d Reference Element (in cell [-1,1])
     type(RefCell) :: ref_cell !! Reference cell
     integer(long) :: max_order
     type(BasisfWrapper), dimension(0:fe_traits%max_order) :: basisf_list
   contains
     procedure, pass(this) :: reinit
     procedure, pass(this) :: eval_basis_functs !! Evaluate the basis functions on a point
     procedure, pass(this) :: eval_dx_basis_functs !! Evaluate x-derivative of b.f. on point
  end type RefElem_Lobatto
  ! interface RefElem_Lobatto
  !    procedure RefElem_Lobatto_ctor
  ! end interface RefElem_Lobatto

  !! Global reference element. Finite elements shall store a pointer to it.
  type(RefElem_Lobatto), target :: the_ref_elem

  ! type(RefElem_Lobatto), parameter, dimension(0:1)& !fe_traits%max_order)&
  !      :: ref_elem_list = [ref_elem, ref_elem] !! List of ref elements

  ! type(BasisfWrapper), dimension(0:fe_traits%max_order) :: basisf_list

  ! = [BasisfWrapper(p1), BasisfWrapper(p1)]
  ! interface
  !    function intrinsic init_basisf_list()
  !      logical init_basisf_list
  !    end function init_basisf_list
  ! end interface
  ! logical, parameter :: basisf_list_initialized = init_basisf_list()

contains

  !! Constructor for BasisFunctionContainer
  ! function BasisfWrapper_ctor(p) result(this)
  !   type(BasisfWrapper) :: this
  !   procedure(basis_f), pointer :: p
  !   this%f => p
  ! end function BasisfWrapper_ctor

  subroutine reinit(this)
    class(RefElem_Lobatto) :: this
    this%ref_cell = fe_traits%ref_cell
    this%max_order = fe_traits%max_order
    if (fe_traits%max_order.ne.7) stop&
         "Error, fe_traits%max_order ha de coincidir con (nº basis functions)-1"
    this%basisf_list(0)%f => phi_0
    this%basisf_list(0)%dx_f => dx_phi_0
    this%basisf_list(1)%f => phi_1
    this%basisf_list(1)%dx_f => dx_phi_1
    this%basisf_list(2)%f => phi_2
    this%basisf_list(2)%dx_f => dx_phi_2
    this%basisf_list(3)%f => phi_3
    this%basisf_list(3)%dx_f => dx_phi_3
    this%basisf_list(4)%f => phi_4
    this%basisf_list(4)%dx_f => dx_phi_4
    this%basisf_list(5)%f => phi_5
    this%basisf_list(5)%dx_f => dx_phi_5
    this%basisf_list(6)%f => phi_6
    this%basisf_list(6)%dx_f => dx_phi_6
    this%basisf_list(7)%f => phi_7
    this%basisf_list(7)%dx_f => dx_phi_7
  end subroutine reinit

  !-------------------------------------------------------------------
  subroutine eval_basis_functs(this, x, output)
    !! Return a 2-dimensional vector where the i-th element (i=1,2)
    !! stores the value of the i-th basis function on a point, x, in [-1,1]
    class(RefElem_Lobatto) :: this
    real(dp), dimension(:), intent(in) :: x
    real(dp), dimension(:,:), intent(out) :: output
    integer(long) :: i,j
    do j = 1, size(output,2)
       do i = 1, size(output,1)
          output(i,j) = this%basisf_list(j-1)%f(x(i))
       end do
    end do
  end subroutine eval_basis_functs

  !-------------------------------------------------------------------
  subroutine eval_dx_basis_functs(this, x, output)
    !! Return a 2-dimensional vector where the i-th element (i=1,2)
    !! stores the value of the i-th basis function on a point, x, in [-1,1]
    class(RefElem_Lobatto) :: this
    real(dp), dimension(:), intent(in) :: x
    real(dp), dimension(:,:), intent(out) :: output
    integer(long) :: i,j
    do j = 1, size(output,2)
       do i = 1, size(output,1)
          output(i,j) = this%basisf_list(j-1)%dx_f(x(i))
       end do
    end do
  end subroutine eval_dx_basis_functs
end module Lobatto_ref_elem_1d


module Legendre_ref_elem_1d
  !! Reference context on the cell (interval) [-1,1], for finite elements 1d
  use nrtypes, only: dp, long !! Double precision and int long numeric types
  use fe_traits_1d, only: RefCell, fe_traits => Legendre_fe_traits
  use Legendre_basisf
  implicit none
  private
  public&
       RefElem_Legendre, &
       the_ref_elem

  abstract interface
    pure function basis_f(x)
      use nrtypes, only: dp
      real(dp) :: basis_f
      real(dp), intent(in) :: x
    end function
  end interface
  type BasisfWrapper
     procedure(basis_f), nopass, pointer :: f
     procedure(basis_f), nopass, pointer :: dx_f
  end type BasisfWrapper

  type RefElem_Legendre
     !! Lagrange 1d Reference Element (in cell [-1,1])
     type(RefCell) :: ref_cell !! Reference cell
     integer(long) :: max_order
     type(BasisfWrapper), dimension(0:fe_traits%max_order) :: basisf_list
   contains
     procedure, pass(this) :: reinit
     procedure, pass(this) :: eval_basis_functs !! Evaluate the basis functions on a point
     procedure, pass(this) :: eval_dx_basis_functs !! Evaluate x-derivative of b.f. on point
  end type RefElem_Legendre
  ! interface RefElem_Legendre
  !    procedure RefElem_Legendre_ctor
  ! end interface RefElem_Legendre

  !! Global reference element. Finite elements shall store a pointer to it.
  type(RefElem_Legendre), target :: the_ref_elem

  ! type(RefElem_Legendre), parameter, dimension(0:1)& !fe_traits%max_order)&
  !      :: ref_elem_list = [ref_elem, ref_elem] !! List of ref elements

  ! type(BasisfWrapper), dimension(0:fe_traits%max_order) :: basisf_list

  ! = [BasisfWrapper(p1), BasisfWrapper(p1)]
  ! interface
  !    function intrinsic init_basisf_list()
  !      logical init_basisf_list
  !    end function init_basisf_list
  ! end interface
  ! logical, parameter :: basisf_list_initialized = init_basisf_list()

contains

  !! Constructor for BasisFunctionContainer
  ! function BasisfWrapper_ctor(p) result(this)
  !   type(BasisfWrapper) :: this
  !   procedure(basis_f), pointer :: p
  !   this%f => p
  ! end function BasisfWrapper_ctor

  subroutine reinit(this)
    class(RefElem_Legendre) :: this
    this%ref_cell = fe_traits%ref_cell
    this%max_order = fe_traits%max_order
    if (fe_traits%max_order.ne.2) stop&
         "Error, fe_traits%max_order ha de coincidir con (nº basis functions)-1"
    this%basisf_list(0)%f => phi_0
    this%basisf_list(0)%dx_f => dx_phi_0
    this%basisf_list(1)%f => phi_1
    this%basisf_list(1)%dx_f => dx_phi_1
    this%basisf_list(2)%f => phi_2
    this%basisf_list(2)%dx_f => dx_phi_2
    ! this%basisf_list(3)%f => phi_3
    ! this%basisf_list(3)%dx_f => dx_phi_3
  end subroutine reinit

  !-------------------------------------------------------------------
  subroutine eval_basis_functs(this, x, output)
    !! Return a 2-dimensional vector where the i-th element (i=1,2)
    !! stores the value of the i-th basis function on a point, x, in [-1,1]
    class(RefElem_Legendre) :: this
    real(dp), dimension(:), intent(in) :: x
    real(dp), dimension(:,:), intent(out) :: output
    integer(long) :: i,j
    do j = 1, size(output,2)
       do i = 1, size(output,1)
          output(i,j) = this%basisf_list(j-1)%f(x(i))
       end do
    end do
  end subroutine eval_basis_functs

  !-------------------------------------------------------------------
  subroutine eval_dx_basis_functs(this, x, output)
    !! Return a 2-dimensional vector where the i-th element (i=1,2)
    !! stores the value of the i-th basis function on a point, x, in [-1,1]
    class(RefElem_Legendre) :: this
    real(dp), dimension(:), intent(in) :: x
    real(dp), dimension(:,:), intent(out) :: output
    integer(long) :: i,j
    do j = 1, size(output,2)
       do i = 1, size(output,1)
          output(i,j) = this%basisf_list(j-1)%dx_f(x(i))
       end do
    end do
  end subroutine eval_dx_basis_functs
end module Legendre_ref_elem_1d

! Local Variables:
! flycheck-gfortran-include-path: ("../src")
! End:
