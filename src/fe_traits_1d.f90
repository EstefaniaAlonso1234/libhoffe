module refcell_1d_module
  !!
  use nrtypes, only : dp
  implicit none
  type RefCell_1d
     !! Define a 1d reference cell
     real(dp), dimension(2) :: vertices !! Vertex of reference cell
  end type RefCell_1d
end module refcell_1d_module

module fe_traits_1d
  use nrtypes, only : dp, long
  use refcell_1d_module, only: RefCell => RefCell_1d ! RefCell is a template parameter
  implicit none
  private
  public &
       FeTraits,&
       RefCell,&
       lagrange_fe_traits,&
       lobatto_fe_traits,&
       legendre_fe_traits

  type FeTraits
     !! Structure used for definition of characteristics of a given FE
     type(RefCell) :: ref_cell
     integer(long) :: geometric_dimension
     integer(long) :: topologic_dimension
     integer(long) :: max_order
  end type FeTraits

  type(FeTraits) :: lagrange_fe_traits !! Specific traits for Lagrange, 1-d FE
  parameter( lagrange_fe_traits = FeTraits(&
       ref_cell = RefCell( vertices=[-1.0_dp, 1.0_dp] ),&
       geometric_dimension=1,&
       topologic_dimension=1,&
       max_order = 1) )

  type(FeTraits) :: lobatto_fe_traits !! Specific traits for Lobatto, 1-d FE
  parameter( lobatto_fe_traits = FeTraits(&
       ref_cell = RefCell( vertices=[-1.0_dp, 1.0_dp] ),&
       geometric_dimension=1,&
       topologic_dimension=1,&
       max_order = 7) )

  type(FeTraits) :: legendre_fe_traits !! Specific traits for Legendre, 1-d FE
  parameter( legendre_fe_traits = FeTraits(&
       ref_cell = RefCell( vertices=[-1.0_dp, 1.0_dp] ),&
       geometric_dimension=1,&
       topologic_dimension=1,&
       max_order = 2) )

end module fe_traits_1d
